
package esinf_1150352_1160678;

import java.util.*;

public class Personagem implements Comparable<Personagem>{
    
    public String nome;
    public int forca;
    public List<Local> locais;//Lista de locais que são da personagem (é dona destes).
    
    public Personagem(String nome, int forca) {
        this.nome = nome;
        this.forca = forca;
        locais = new ArrayList<>();
    }
    
    /**
     * Adicionar um local á lista de locais da personagem.
     * @param l Local a adicionar
     * @return boolean que indica se este foi adicionado com sucesso
     */
    public boolean addLocal(Local l){
        if(l == null) return false;
        getLocais().add(l);
        return true;
    }
    
    /**
     * Remove um locais da lista
     * @param l Local a remover
     * @return boolean que indica se este foi removido com sucesso
     */
    public boolean removeLocal(Local l) {
        if(l == null) return false;
        getLocais().remove(l);
        return true;
    }
    
    /**
     * equals reescrito.
     * Uma personagem é igual quando os seus nomes são iguais.
     * @param o Objeto a comparar
     * @return boolean que indica se estes são iguais.
     */
    @Override
    public boolean equals(Object o){
        if(o == null || this.getClass() != o.getClass()) return false;
        
        Personagem outraPersonagem = (Personagem) o;
        return this.nome.equals(outraPersonagem.nome);
    }
    
    /**
     * toString reescrito para melhor leitura do objeto.
     * @return 
     */
    @Override
    public String toString(){
        return "Personagem: " +
                "\nNome: " + nome +
                "\nForca: " + forca;
    }
    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @return the forca
     */
    public int getForca() {
        return forca;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @param forca the forca to set
     */
    public void setForca(int forca) {
        this.forca = forca;
    }

    /**
     * @return the locais
     */
    public List<Local> getLocais() {
        return locais;
    }

    /**
     * @param locais the locais to set
     */
    public void setLocais(List<Local> locais) {
        this.locais = locais;
    }

    @Override
    public int compareTo(Personagem t) {
        String nome = this.getNome();
        String tNome = t.getNome();
        return nome.compareTo(tNome);
    }
}

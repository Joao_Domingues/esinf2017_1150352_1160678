package esinf_1150352_1160678;

import graphbase.Edge;
import graphbase.Graph;
import graphbase.GraphAlgorithms;
import java.util.*;

public class MapaAliancas {

    public Graph<Personagem, Alianca> mapaAliancas;

    public MapaAliancas() {
        mapaAliancas = new Graph<>(false);//não é direcionado
    }
    
    /**
     * Método que vai buscar um edge entre duas personagens.
     * @param p1 Personagem origem/destino
     * @param p2 Personagem origem/destino
     * @return O edge entre as duas personagens.
     */
    public Edge<Personagem, Alianca> getEdge(Personagem p1, Personagem p2) {
        Edge<Personagem, Alianca> e = null;
        ArrayList<Edge<Personagem, Alianca>> listaEdges = (ArrayList<Edge<Personagem, Alianca>>) getMapaAliancas().edges();
        for (Edge<Personagem, Alianca> edge : listaEdges) {
            if ((edge.getVOrig().equals(p1) && edge.getVDest().equals(p2)) || (edge.getVOrig().equals(p2) && edge.getVDest().equals(p1))) {
                e = edge;
            }
        }
        return e;
    }
    
    /**
     * Método apenas para remover um edge
     * @param p1
     * @param p2
     * @return 
     */
    public boolean removeEdge(Personagem p1, Personagem p2) {
        return getMapaAliancas().removeEdge(p1, p2);
    }
    
    /**
     * Método de inserir personagem (vertice).
     * @param p Personagem a ser inserida
     * @return boolean que indica se esta foi adicionada ou não
     */
    public boolean inserirPersonagem(Personagem p) {
        if (p == null) {
            return false;
        }

        return getMapaAliancas().insertVertex(p);
    }
    
    /**
     * Método que insere uma alianca (edge).
     * @param nomeP1 Vertice de origem
     * @param nomeP2 Vertice de destino
     * @param tipo booleano com a privacidade da alianca
     * @param compatibilidade Compatibilidade da alianca
     * @return boolean que indica se alianca foi inserida com sucesso.
     */
    public boolean inserirAlianca(String nomeP1, String nomeP2, boolean tipo, double compatibilidade) {
        if (nomeP1 == null || nomeP2 == null || compatibilidade <= 0 || compatibilidade > 1) {
            return false;
        }

        Personagem p1 = getPersonagemOfNome(nomeP1);
        Personagem p2 = getPersonagemOfNome(nomeP2);
        if (p1 == null || p2 == null) {
            return false;
        }

        Alianca a = new Alianca(p1, p2, tipo, compatibilidade);
        double forca = (double) ((p1.forca + p2.forca) * compatibilidade);

        return getMapaAliancas().insertEdge(p1, p2, a, forca);
    }
    
    /**
     * Método para ir buscar um personagem através do seu nome.
     * @param nome Nome da personagem que se quer encontrar
     * @return A personagem correspondente ao nome
     */
    public Personagem getPersonagemOfNome(String nome) {
        Iterator<Personagem> it = getMapaAliancas().vertices().iterator();
        Personagem p = null;
        while (it.hasNext()) {
            Personagem pAtual = it.next();
            if (pAtual.nome.equals(nome)) {
                p = pAtual;
            }
        }
        return p;
    }

    /**
     * Método que devolve os aliados de uma personagem, ou seja os seus vertices
     * adjacentes.
     *
     * @param p Personagem de quem queremos saber os aliados.
     * @return Linked List de personagens aliadas de p.
     */
    public LinkedList<Personagem> aliados(Personagem p) {
        if (!mapaAliancas.validVertex(p)) {
            return null;
        }

        LinkedList<Personagem> pAliados = new LinkedList<>();

        Iterable<Personagem> source = getMapaAliancas().adjVertices(p);

        source.forEach(pAliados::add);

        if (pAliados.isEmpty()) {
            return null;
        }

        return pAliados;
    }

    /**
     * Método que procura a aliança mais forte nos edges do mapa das aliancas.
     *
     * @return A força e as duas personagens da aliança num array de strings.
     */
    public LinkedList<Alianca> aliancaMaisForte() {
        LinkedList<Alianca> maisFortes = new LinkedList<>();
        ArrayList<Edge<Personagem, Alianca>> lista = (ArrayList<Edge<Personagem, Alianca>>) getMapaAliancas().edges();
        if (lista.isEmpty()) {
            return null;
        }

        Iterator<Edge<Personagem, Alianca>> eIt = getMapaAliancas().edges().iterator();
        Edge<Personagem, Alianca> aliancaMaisForte = eIt.next();
        while (eIt.hasNext()) {
            Edge<Personagem, Alianca> aliancaAtual = eIt.next();
            if (aliancaAtual.getWeight() > aliancaMaisForte.getWeight()) {
                aliancaMaisForte = aliancaAtual;
            }
        }

        maisFortes.add(aliancaMaisForte.getElement());

        Iterator<Edge<Personagem, Alianca>> novoIt = getMapaAliancas().edges().iterator();
        while (novoIt.hasNext()) { //ver se há iguais
            Edge<Personagem, Alianca> comp = novoIt.next();
            if (!maisFortes.contains(comp.getElement()) && comp.getWeight() == aliancaMaisForte.getWeight()) {
                maisFortes.add(comp.getElement());
            }
        }
        return maisFortes;
    }

    /**
     * Método para a criação de uma nova aliança entre duas personagens.
     *
     * @param a Personagem da aliança
     * @param b Personagem a ser aliada.
     * @return Boolean que indica se a aliança foi criada com sucesso ou não.
     */
    public boolean criarAlianca(Personagem a, Personagem b) {
        if (!mapaAliancas.validVertex(a) || !mapaAliancas.validVertex(b)) {
            return false;
        }

        LinkedList<Personagem> aAliados = aliados(a);

        if (aAliados != null) {
            Iterator<Personagem> aIt = aAliados.iterator();
            while (aIt.hasNext()) {
                Personagem aliadoAtual = aIt.next();
                if (aliadoAtual.equals(b)) {
                    return false; //já é aliado
                }
            }
        }

        Random r = new Random();
        Graph<Personagem, Alianca> clone = getMapaAliancas().clone();
        ArrayList<Edge<Personagem, Alianca>> edgesTotal = (ArrayList<Edge<Personagem, Alianca>>) clone.edges();

        //colocar o peso igual em todos para o shortest path não ser influenciado
        for (Edge<Personagem, Alianca> edge : edgesTotal) {
            edge.setWeight(1); //para o shortest path não ter em conta os pesos do edge.
        }

        for (Edge<Personagem, Alianca> e : edgesTotal) { //remover aliancas privadas
            if (e.getElement().tipo == false && !a.nome.equals(e.getVOrig().nome) && !a.nome.equals(e.getVDest().nome)) {
                clone.removeEdge(e.getVOrig(), e.getVDest());
            }
        }

        LinkedList<Personagem> shortestPath = new LinkedList<>();
        double existsAlianca = GraphAlgorithms.shortestPath(clone, a, b, shortestPath); //fazer o shortest path independente dos pesos dos edges
        if (existsAlianca <= 0) { //b não está na rede de alianças de a
            double[] compatibilidades = {0.2, 0.5, 0.8};
            int index = r.nextInt(compatibilidades.length); //o indice vai ser random entre 3 valores
            double compatibilidade = compatibilidades[index];//valores random entre os tres valores do array
            return inserirAlianca(a.nome, b.nome, true, compatibilidade);
        }

        //Se b estiver na rede de alianças de a
        Iterator<Personagem> pIt = shortestPath.iterator();
        Iterator<Personagem> pIt2 = shortestPath.iterator();
        pIt2.next();
        double totalComp = 0;
        int count = 0;
        while (pIt2.hasNext()) { //Iterar o shortest path
            Personagem p1 = pIt.next();
            Personagem p2 = pIt2.next();
            ArrayList<Edge<Personagem, Alianca>> edgesTotal1 = (ArrayList<Edge<Personagem, Alianca>>) getMapaAliancas().edges();
            for (Edge<Personagem, Alianca> ed : edgesTotal1) { //iterar os edges totais do mapa normal para ir buscar a alianca que pertence aos personagens do shortestPath
                if ((ed.getVOrig().equals(p1) && ed.getVDest().equals(p2)) || (ed.getVOrig().equals(p2) && ed.getVDest().equals(p1))) {
                    totalComp = totalComp + ed.getElement().getCompatibilidade(); //soma das compatibilidades
                    count++; //contar os caminhos para fazer a média das compatibilidades
                }
            }
        }
        double compMedia = (double) (totalComp / count); //média das compatibilidades com o número de caminhos do shortestPath

        return inserirAlianca(a.nome, b.nome, true, compMedia);
    }
    
    /**
     * Método que cria um clone do grafo, mas com todas as alianças possiveis ligadas entre si.
     * Sendo que cada aliança criada vai ter uma compatibilidade aleatória e a sua força (peso do ramo) calculada através dessa compatibilidade 
     * @return clone do grafo com todas as alianças possíveis criadas.
     */
    public Graph<Personagem, Alianca> graphAllPossibleAliancas() { //tentar usar shortest path para ver se é alcançavel e adjVertices para ver se já não é um adjacente e assim cobrir todas as possibilidades.
        Graph<Personagem, Alianca> clone = getMapaAliancas().clone();
        ArrayList<Edge<Personagem, Alianca>> edgesTotais = (ArrayList<Edge<Personagem, Alianca>>) getMapaAliancas().edges();
        if (edgesTotais.isEmpty()) {
            return null;
        }
        for (Edge<Personagem, Alianca> e : edgesTotais) {
            LinkedList<Personagem> aliados = new LinkedList<>();
            Personagem p = e.getVOrig();
            aliados = aliados(p); //quando chegar ao ultimo este ja vai ter todas as ligacoes
            for (Personagem outraPersonagem : getMapaAliancas().vertices()) {
                if (!aliados.contains(outraPersonagem) && !p.equals(outraPersonagem)) {
                    LinkedList<Personagem> caminhoMinimo = new LinkedList<>();
                    double length = GraphAlgorithms.shortestPath(clone, p, outraPersonagem, caminhoMinimo);
                    if (length > 0) { //caminho valido
                        Random r = new Random();
                        double[] compatibilidades = {0.2, 0.5, 0.8};
                        int index = r.nextInt(compatibilidades.length);
                        double forca = (double) ((p.forca + outraPersonagem.forca) * compatibilidades[index]);
                        clone.insertEdge(p, outraPersonagem, new Alianca(p, outraPersonagem, true, compatibilidades[index]), forca);
                    }
                }
            }
        }
        return clone;
    }

    /**
     * @return the mapaAliancas
     */
    public Graph<Personagem, Alianca> getMapaAliancas() {
        return mapaAliancas;
    }
}

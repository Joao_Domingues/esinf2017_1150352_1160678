
package esinf_1150352_1160678;

import graph.GraphAlgorithms;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.TreeMap;

public class ControloJogo {
    
    private MapaAliancas mapaAliancas;
    private MapaEstradas mapaEstradas;
    
    public ControloJogo() {
        mapaEstradas = new MapaEstradas();
        mapaAliancas = new MapaAliancas();
    }
    
    public ControloJogo(MapaAliancas mapaAliancas, MapaEstradas mapaEstradas) {
        this.mapaAliancas = mapaAliancas;
        this.mapaEstradas = mapaEstradas;
    }
    
    public MapaEstradas getMapaEstradas() {
        return this.mapaEstradas;
    }
    
    public MapaAliancas getMapaAliancas() {
        return this.mapaAliancas;
    }
    
    /**
     * Verifica qual o melhor caminho a percorrer para conquistar determinado local, juntamente com o aliado a ajudá-lo
     * Primeiro, determina quais os aliados que podem ajudar a personagem, ao ir buscar a lista de aliados e removendo o aliado que é dono do local a conquistar, caso se aplique
     * Segundo, cria uma lista com todos os caminhos existentes entre todos os locais pertencentes à personagem e o local a conquistar (esta lista é clonada para poder ser usada pelos vários aliados, sem ter que se fazer a pesquisa uma vez por cada aliado)
     * Terceiro, itera por todos os aliados disponíveis para ajudar (pelos passos 4, 5 e 6) - para cada aliado, remove da lista completa de caminhos completa (mais propriamente o clone da mesma) os caminhos que não podem ser percorridos juntamente com esse aliado, seja a razão a falta de força por parte da aliança ou qualquer dos membros da aliança, ou o facto que o caminho contém um local pertencente à personagem ou ao aliado
     * Quarto, dos caminhos restantes, determina quais os que têm menor comprimento em termos de número de locais por onde passa (nota*)
     * Quinto, dos caminhos restantes, determina qual o mais curto em termos de força necessária a atravessar
     * Sexto, mete num treemap<Personagem, Object[] = {int, LinkedList<Local>} o aliado a ser iterado, juntamente com a força necessária para percorrer o caminho mais eficiente e qual o caminho (nota*, caso não haja caminhos percorríveis juntamente com o aliado, o caminho selecionado será o caminho mais eficiente, seguindo o mesmo algoritmo, mas ignorando a força da aliança e respetivas personagens, sendo que a respetiva força será apresentada em negativo)
     * Sétimo, itera pelos aliados do treemap, determinando, dos respetivos caminhos, qual o aliado que tem o caminho mais eficiente a percorrer
     * Oitavo, retorna um Object[int, Personagem], sendo que o int é representativo da força necessária para percorrer o caminho final seleciona e a Personagem é o respetivo aliado.
     * Caso a força necessária seja 0 ou negativo, a Personagem retornada será null
     * @param personagem
     * @param local
     * @param caminhoFinal
     * @return Object[] com a força necessária para percorrer o caminho e o aliado com quem se percorre
     */
    public Object[] personagemPodeConquistarLocalComAliado(Personagem personagem, Local local, LinkedList<Local> caminhoFinal) {
        
        Object[] resultadoFinal = {0, null};
        if(mapaEstradas == null || local == null || personagem == null) return resultadoFinal;
        if(!mapaEstradas.getMapaEstradas().checkVertex(local)) return resultadoFinal;
        if(local.getPersonagem() != null) { 
            if(local.getPersonagem().equals(personagem)) return resultadoFinal;
        }
        if(caminhoFinal != null) caminhoFinal.clear();
        if(caminhoFinal == null) caminhoFinal = new LinkedList<>();
        
        LinkedList<Personagem> listaAliados = new LinkedList<>();
        
        // vai buscar os aliados da personagem
        listaAliados = mapaAliancas.aliados(personagem);
        
        // se a personagem não tiver aliados
        if(listaAliados == null) return resultadoFinal;
        if(listaAliados.isEmpty()) return resultadoFinal;
        
        // verifica se algum aliado é dono do local a conquistar, removendo-o da lista
        Personagem aliadoAEliminar = null;
        for(Personagem aliado : listaAliados) {
            if(local.getPersonagem() != null) {
                if(local.getPersonagem().equals(aliado)) aliadoAEliminar = aliado;
            } 
        }
        if(aliadoAEliminar != null) listaAliados.remove(aliadoAEliminar);
        if(listaAliados.isEmpty()) return resultadoFinal;
        // determina todos os caminhos entre todos os locais da personagem e o local a conquistar
        LinkedList<LinkedList<Local>> todosOsCaminhosEntrePersonagemELocal = new LinkedList<>();

        for(Local localP : personagem.getLocais()) {
            GraphAlgorithms.allPaths(mapaEstradas.getMapaEstradas(), localP, local, todosOsCaminhosEntrePersonagemELocal);
        }
        
        if(todosOsCaminhosEntrePersonagemELocal.isEmpty()) return resultadoFinal;
        
        LinkedList<LinkedList<Local>> listaDeCaminhosAEliminar = new LinkedList<>();
        
        // treemap que vai conter todos os caminhos possiveis de percorrer associados ao aliado com quem são percorridos, juntamente com a força necessária para percorrer cada um dos caminhos
        Map<Personagem, Object[]> mapaCaminhos = new TreeMap<>();
        // array de objects (int, LinkedList)
        Object[] intCaminho = new Object[2];
        int forcaNecessaria;
        
        // determina o caminho mais eficiente para cada aliança
        for(Personagem aliado : listaAliados) {
            LinkedList<LinkedList<Local>> listaDeCaminhosClone = (LinkedList) todosOsCaminhosEntrePersonagemELocal.clone();
            
            // remove os caminhos que não são  percorríveis por falta de força
            for(LinkedList<Local> caminho : todosOsCaminhosEntrePersonagemELocal) {
                if(!caminhoEPercorrivelPorAlianca(caminho, personagem, aliado)) listaDeCaminhosAEliminar.add(caminho);
            }
            listaDeCaminhosClone.removeAll(listaDeCaminhosAEliminar);
            
            // verifica se ainda há caminhos
            if(!listaDeCaminhosClone.isEmpty()) { 

                forcaNecessaria = personagemPodeConquistarLocalComAliadoAuxiliar(caminhoFinal, listaDeCaminhosClone);

            } else {

                listaDeCaminhosClone = (LinkedList) todosOsCaminhosEntrePersonagemELocal.clone();
                listaDeCaminhosAEliminar.clear();
                
                // remove os caminhos que contêm locais que o dono seja o aliado em consideração
                for(LinkedList<Local> caminho : listaDeCaminhosClone) {
                    if(!removerCaminhosComAliado(caminho, personagem, aliado)) listaDeCaminhosAEliminar.add(caminho);
                }
                listaDeCaminhosClone.removeAll(listaDeCaminhosAEliminar);
                
                if(listaDeCaminhosClone.isEmpty()) {
                    forcaNecessaria = 0;
                    caminhoFinal = null;
                } else {
                    // dá return ao valor da força necessária para percorrer o caminho mais favorável, em negativo
                    forcaNecessaria = -personagemPodeConquistarLocalComAliadoAuxiliar(caminhoFinal, todosOsCaminhosEntrePersonagemELocal);
                }
            }
            
            intCaminho[0] = forcaNecessaria;
            intCaminho[1] = caminhoFinal;
            
            // verifica se o aliado já existe no treemap, e adiciona o array ao respetivo aliado
            if(mapaCaminhos.keySet().contains(aliado)) {
                mapaCaminhos.replace(aliado, intCaminho);
            } else {
                mapaCaminhos.put(aliado, intCaminho);
            }
            
        }
        
        resultadoFinal[0] = Integer.MIN_VALUE;
        
        // determina qual o melhor resultado
        for(Personagem aliado : mapaCaminhos.keySet()) {
            Object[] arrayFinal = mapaCaminhos.get(aliado);
            caminhoFinal = (LinkedList) arrayFinal[1];
            if(caminhoFinal != null && caminhoFinal.size() > (int) resultadoFinal[0] && (int) resultadoFinal[0] < 0 && (int) resultadoFinal[0] != 0) {
                resultadoFinal[0] = arrayFinal[0];
                resultadoFinal[1] = aliado;
            }
        }
        
        double forcaAlianca = Double.MIN_VALUE;
        
        if((int) resultadoFinal[0] == Integer.MIN_VALUE) {
            resultadoFinal[0] = 0;
            resultadoFinal[1] = null;
        } 
        
        if((int) resultadoFinal[0] < 0) {
            for(Personagem aliado : mapaCaminhos.keySet()) {
                if(forcaDaAlianca(personagem, aliado) > forcaAlianca) {
                    forcaAlianca = forcaDaAlianca(personagem, aliado);
                }
            }
            resultadoFinal[1] = null;
        }
        
        return resultadoFinal;
    }
    
    /**
     * Determina de uma lista de caminhos, qual o mais favorável
     * Primeiro escolhe os mais curtos em termos de número de Locais
     * Depois, dos restantes, escolhe qual o que requer menos força para ser percorrido
     * @param caminhoFinal
     * @param todosOsCaminhosEntrePersonagemELocal
     * @return valor da força necessária para percorrer o caminho mais favorável
     */
    private int personagemPodeConquistarLocalComAliadoAuxiliar(LinkedList<Local> caminhoFinal, LinkedList<LinkedList<Local>> todosOsCaminhosEntrePersonagemELocal) {
        int size = Integer.MAX_VALUE;
        LinkedList<LinkedList<Local>> caminhosMaisCurtos = new LinkedList<>();

        // verifica quais os caminhos mais curto dos restantes  (em termos de número de locais e não em quantidade de força necessária)
        for(LinkedList<Local> caminho : todosOsCaminhosEntrePersonagemELocal) {
            if(caminho.size() < size) {
                caminhosMaisCurtos.clear();
                caminhosMaisCurtos.add(caminho);
                size = caminho.size();
            } else if (caminho.size() == size) {
                caminhosMaisCurtos.add(caminho);
            }
        }

        // dos caminhos restantes verifica qual o que tem menor requerimento de força para ser percorrido
        size = Integer.MAX_VALUE;
        int forcaNecessaria;
        for(LinkedList<Local> caminho : caminhosMaisCurtos) {
            forcaNecessaria = mapaEstradas.forcaNecessariaParaPercorrerCaminho(caminho);
            if(forcaNecessaria < size) {
                caminhoFinal.clear();
                caminhoFinal.addAll(caminho);
                size = forcaNecessaria;
            }
        }

        // determina a força necessária para percorrer o caminho percorrível mais curto em termos de número de locais
        return size;
    }
    
    /**
     * Retorna o valor da força de uma aliança entre duas personagens
     * @param p1
     * @param p2
     * @return força da aliança entre duas personagens, caso exista. -1 se a aliança não for válida
     */
    public double forcaDaAlianca(Personagem p1, Personagem p2) {
        if(p1 == null || p2 == null) return -1;
        if(mapaAliancas.aliados(p1) == null) return -1;
        if(!mapaAliancas.aliados(p1).contains(p2)) return -1;
        
        return mapaAliancas.getEdge(p1, p2).getWeight();
    }
    
    /**
     * Verifica se um caminho é percorrível por uma dada aliança
     * @param caminho
     * @param personagem
     * @param aliado
     * @return 
     */
    public boolean caminhoEPercorrivelPorAlianca(LinkedList<Local> caminho, Personagem personagem, Personagem aliado) {
        if(personagem == null || aliado == null) return false;
        if(caminho == null) return false;
        if(caminho.isEmpty()) return false;
        
        double forcaAlianca = forcaDaAlianca(personagem, aliado);
        if(forcaAlianca == -1) return false;
        
        if(!removerCaminhosComAliado(caminho, personagem, aliado)) return false;
        
        int forcaNecessaria = mapaEstradas.forcaNecessariaParaPercorrerCaminho(caminho);
        if(forcaNecessaria == -1) return false;
        
        int forcaPerdida = forcaNecessaria / 2;
        if(forcaPerdida > personagem.getForca()) return false;
        if(forcaPerdida > aliado.getForca()) return false;
        
        if(forcaNecessaria >= forcaAlianca) return false;
        
        return true;
    }
    
    /**
     * Verifica se um caminho contém locais pertencentes ao aliado em consideração
     * @param caminho
     * @param personagem
     * @param aliado
     * @return 
     */
    private boolean removerCaminhosComAliado(LinkedList<Local> caminho, Personagem personagem, Personagem aliado) {
        Iterator<Local> it = caminho.iterator();
        
        while(it.hasNext()) {
            Local local = it.next();
            if(local.getPersonagem() != null && local.getPersonagem().equals(aliado)) return false;
        } 
        
        return true;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esinf_1150352_1160678;

import graphbase.*;
import graph.*;
import java.util.LinkedList;

/**
 *
 * @author HP
 */
public class Main {

    public static void main(String[] args) throws Exception {
        MapaEstradas me = new MapaEstradas();
        MapaAliancas ma = new MapaAliancas();

        LeituraFicheiros lfLocais = new LeituraFicheiros(me);

        boolean flag = lfLocais.lerLocaisCaminhos("locais_S.txt");//Ler primeiro os locais e caminhos para depois associarmos ás personagens
        if (flag == false) {
            System.out.println("Ficheiro não encontrado.");
        }
        me = lfLocais.getMe();

        System.out.println("LOCAIS: ");
        System.out.println("------------------------\n");
        //Mostrar Locais.
        for (Local l : me.getMapaEstradas().vertices()) {
            System.out.println(l.toString());
        }
        System.out.println("\n\nCAMINHOS: ");
        System.out.println("------------------------\n");
        //Mostrar dificuldade dos caminhos.
        for (Double forca : me.getMapaEstradas().edges()) {
            System.out.println("Dificuldade do caminho: " + forca);
        }

        LeituraFicheiros lfPersonagens = new LeituraFicheiros(me);

        flag = lfPersonagens.lerPersonagensAliancas("pers_S.txt");
        if (flag == false) {
            System.out.println("Ficheiro não encontrado.");
        }
        ma = lfPersonagens.getMa();

        System.out.println("\n\nPERSONAGENS: ");
        System.out.println("-------------------------\n");
        //Mostrar Personagens
        for (Personagem p : ma.getMapaAliancas().vertices()) {
            System.out.println(p.toString());
        }
        System.out.println("\n\nALIANCAS: ");
        System.out.println("--------------------------\n");
        //Mostrar Alianças e sua força
        for (Edge<Personagem, Alianca> e : ma.getMapaAliancas().edges()) {
            System.out.println(e.getElement().toString());
            System.out.println("Forca: " + e.getWeight());
        }

        System.out.println("\n\nLOCAIS DE CADA PERSONAGEM: ");
        System.out.println("---------------------------\n");
        for (Personagem p : ma.getMapaAliancas().vertices()) {
            System.out.println("Nome: " + p.nome);
            for (Local l : p.locais) {
                System.out.println("Local: " + l.nome);
            }
        }
        System.out.println("\n\nMétodos do mapa de estradas: ");
        System.out.println("---------------------------------\n");

        LinkedList<String> caminhoMenorDificuldade = new LinkedList<>();
        Local lOrig = me.getLocalOfNome("Local1");
        Local lDest = me.getLocalOfNome("Local18");
        caminhoMenorDificuldade = me.caminhoMenorDificuldadeToString(lOrig, lDest);
        if (caminhoMenorDificuldade == null) {
            System.out.println("Caminho não encontrado.");
        }
        System.out.println("CAMINHO MENOR DIFICULDADE ENTRE DOIS LOCAIS:\n");
        for (String local : caminhoMenorDificuldade) {
            System.out.println(local);
        }

        System.out.println("\n\nMétodos do mapa de alianças: ");
        System.out.println("\n\nALIADOS DE UMA PERSONAGEM: ");
        System.out.println("------------------------------------\n");
        
        Personagem p1 = ma.getPersonagemOfNome("Pers2");
        LinkedList<Personagem> aliados = ma.aliados(p1);
        for(Personagem p : aliados){
            System.out.println(p.toString());
        }
        
        System.out.println("\n\nALIANÇAS MAIS FORTES: ");
        System.out.println("-------------------------------------\n");
        
        LinkedList<Alianca> aliancasMaisFortes = ma.aliancaMaisForte();
        for(Alianca a : aliancasMaisFortes){
            System.out.println(a.toString());
        }
        
        Personagem p2 = ma.getPersonagemOfNome("Pers0");
        Personagem p3 = ma.getPersonagemOfNome("Pers2");
        
        boolean flag1 = ma.criarAlianca(p2, p3);
        Edge<Personagem, Alianca> e = ma.getEdge(p2, p3);
        
        System.out.println("\n\nCRIAR UMA ALIANÇA: ");
        System.out.println("--------------------------------------\n");
        
        System.out.println("Alianca criada? " + flag1);
        System.out.println("\nAlianca criada: \n" + e.getElement().toString());
        System.out.println("\nForca da alianca: " + e.getWeight());
        
        Graph<Personagem, Alianca> todasAliancas = ma.graphAllPossibleAliancas();
        
        System.out.println("\n\nGRAFO COM TODAS AS ALIANÇAS POSSÍVEIS: ");
        System.out.println("----------------------------------------\n");
        System.out.println("Alianças do grafo criado: \n");
        for(Edge<Personagem, Alianca> edge : todasAliancas.edges()){
            System.out.println(edge.getElement().toString());
        }
    }
}

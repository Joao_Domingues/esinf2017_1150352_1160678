/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esinf_1150352_1160678;

/**
 *
 * @author HP
 */
public class Local {

    public String nome;
    public Personagem personagem;
    public int pontos;

    public Local(String nome, Personagem personagem, int pontos) {
        this.nome = nome;
        this.personagem = personagem;
        this.pontos = pontos;
    }

    public Local(String nome, int pontos) {
        this.nome = nome;
        this.pontos = pontos;
        this.personagem = null;
    }
    
    /**
     * equals reescrito.
     * Dois locais são iguais quando o seu nome é igual
     * @param o Objeto para comparar
     * @return boolean que indica se os dois objetos são iguais.
     */
    @Override
    public boolean equals(Object o){
        if(o == null) return false;
        if(o.getClass() != this.getClass()) return false;
        
        Local outroLocal = (Local) o;
        return this.nome.equals(outroLocal.nome);
    }
    
    /**
     * toString reescrito para facilitar a leitura do objeto.
     * @return 
     */
    @Override
    public String toString(){
        return "Local: " +
                "\nNome: " + nome +
                "\nForca: " + pontos;
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @return the personagem
     */
    public Personagem getPersonagem() {
        return personagem;
    }

    /**
     * @return the pontos
     */
    public int getPontos() {
        return pontos;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @param personagem the personagem to set
     */
    public void setPersonagem(Personagem personagem) {
        this.personagem = personagem;
    }

    /**
     * @param pontos the pontos to set
     */
    public void setPontos(int pontos) {
        this.pontos = pontos;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esinf_1150352_1160678;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 *
 * @author HP
 */
public class LeituraFicheiros {

    private Scanner sc;
    public MapaEstradas me;
    public MapaAliancas ma;

    public LeituraFicheiros(MapaEstradas me1) {
        this.me = me1;
        ma = new MapaAliancas();
    }
    
    /**
     * Método para ler o ficheiro que contém os locais e os caminhos.
     * @param nomeFich Nome do ficheiro que se pretende ler
     * @return boolean que indica se o ficheiro foi lido corretamente
     * @throws Exception Exceção lançada quando o nome recebido do ficheiro não é válido ou o ficheiro não se encontra no diretório atual.
     */
    public boolean lerLocaisCaminhos(String nomeFich) throws Exception {
        boolean flag = true;
        try {
            sc = new Scanner(new File(nomeFich));
            sc.nextLine();
            String linha = sc.nextLine();

            while (sc.hasNext() && !linha.equals("CAMINHOS")) {
                String[] dados = linha.split(",");
                Local l = new Local(dados[0], Integer.parseInt(dados[1]));
                me.inserirLocal(l);
                linha = sc.nextLine();
            }

            while (sc.hasNext()) {
                linha = sc.nextLine();
                String[] dados = linha.split(",");
                String l1 = dados[0];
                String l2 = dados[1];
                double peso = Double.valueOf(dados[2]);
                me.inserirCaminho(l1, l2, peso);
            }
        } catch (FileNotFoundException | NumberFormatException ex) {
            flag = false;
            System.out.println(ex.getMessage());
        }
        return flag;
    }
    
    /**
     * Método de leitura que lê as personagens e as aliancas.
     * @param nomeFich Nome do ficheiro que se pretende ler.
     * @return boolean que indica se o ficheiro foi lido corretamente
     * @throws Exception Exceção lançada quando o nome do ficheiro recebido não é encontrado ou é inválido.
     */
    public boolean lerPersonagensAliancas(String nomeFich) throws Exception {
        boolean flag = true;
        try {
            sc = new Scanner(new File(nomeFich));
            sc.nextLine();
            String linha = sc.nextLine();

            while (sc.hasNext() && !linha.equals("ALIANÇAS")) {
                String[] dados = linha.split(",");
                Personagem p = new Personagem(dados[0], Integer.valueOf(dados[1]));
                Local lp = getMe().getLocalOfNome(dados[2]);
                lp.setPersonagem(p);
                p.locais.add(lp);
                ma.inserirPersonagem(p);
                linha = sc.nextLine();
            }

            while (sc.hasNext()) {
                linha = sc.nextLine();
                String[] dados = linha.split(",");
                ma.inserirAlianca(dados[0], dados[1], Boolean.valueOf(dados[2]), Double.valueOf(dados[3]));
            }
        } catch (NumberFormatException | FileNotFoundException ex) {
            flag = false;
            System.out.println(ex.getMessage());
        }
        return flag;
    }

    /**
     * @return the me
     */
    public MapaEstradas getMe() {
        return me;
    }

    /**
     * @return the ma
     */
    public MapaAliancas getMa() {
        return ma;
    }
}

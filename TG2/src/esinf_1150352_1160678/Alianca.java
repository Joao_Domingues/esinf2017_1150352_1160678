/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esinf_1150352_1160678;

/**
 *
 * @author HP
 */
public class Alianca {

    public Personagem personagem1;
    public Personagem personagem2;
    public double compatibilidade;

    /**
     * false = privado, true = publico
     */
    public boolean tipo;

    public Alianca(Personagem personagem1, Personagem personagem2, boolean tipo, double compatibilidade) {
        this.personagem1 = personagem1;
        this.personagem2 = personagem2;
        this.tipo = tipo;
        this.compatibilidade = compatibilidade;
    }
    
    /**
     * Equals reescrito.
     * Uma aliança só é igual quando ambas as suas personagens são iguais.
     * @param o Objeto a ser comparado
     * @return boolean que indica se os dois objetos são iguais.
     */
    @Override
    public boolean equals(Object o){
        if(o == null || this.getClass() != o.getClass()) return false;
        
        Alianca outraAlianca = (Alianca) o;
        return outraAlianca.personagem1.equals(this.personagem1) && outraAlianca.personagem2.equals(this.personagem2);
    }
    
    /**
     * toString reescrito para melhor leitura dos objetos.
     * @return 
     */
    @Override
    public String toString(){
        return "Alianca: " + 
                "\nPersonagem1: " + personagem1.nome +
                "\nPersonagem2: " + personagem2.nome + 
                "\nCompatibilidade: " + compatibilidade +
                "\nTipo: " + tipo;
    }

    /**
     * @return the personagem1
     */
    public Personagem getPersonagem1() {
        return personagem1;
    }

    /**
     * @return the personagem2
     */
    public Personagem getPersonagem2() {
        return personagem2;
    }

    /**
     * @return the compatibilidade
     */
    public double getCompatibilidade() {
        return compatibilidade;
    }

    /**
     * @return the tipo
     */
    public boolean isTipo() {
        return tipo;
    }

    /**
     * @param compatibilidade the compatibilidade to set
     */
    public void setCompatibilidade(double compatibilidade) {
        this.compatibilidade = compatibilidade;
    }
}

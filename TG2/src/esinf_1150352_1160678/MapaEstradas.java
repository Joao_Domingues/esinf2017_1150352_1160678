package esinf_1150352_1160678;

import graph.AdjacencyMatrixGraph;
import graph.EdgeAsDoubleGraphAlgorithms;
import graph.GraphAlgorithms;
import java.util.*;

public class MapaEstradas {

    private AdjacencyMatrixGraph<Local, Double> mapaEstradas;

    public MapaEstradas() {
        mapaEstradas = new AdjacencyMatrixGraph<>();
    }

    /**
     * Método para inserir um local (vertice)
     * @param l Local a ser inserido
     * @return boolean que indica se o local foi inserido com sucesso
     */
    public boolean inserirLocal(Local l) {
        if (l == null) {
            return false;
        }
       return mapaEstradas.insertVertex(l);
    }
    
    public AdjacencyMatrixGraph<Local, Double> getMapaEstradas() {
        return this.mapaEstradas;
    }  
    
    /**
     * Método para inserir um caminho (edge)
     * @param l1 Nome do primeiro local
     * @param l2 Nome do segundo local
     * @param peso dificuldade de percorrer o caminho
     * @return boolean que indica se o caminho foi inserido com sucesso.
     */
    public boolean inserirCaminho(String l1, String l2, double peso) {
        if (l1 == null || l2 == null || peso < 0) {
            return false;
        }

        Local local1 = getLocalOfNome(l1);
        Local local2 = getLocalOfNome(l2);
        if (local1 == null || local2 == null) {
            return false;
        }

        if (!mapaEstradas.checkVertex(local1) && !mapaEstradas.checkVertex(local2)) {
            return false;
        }

        return mapaEstradas.insertEdge(local1, local2, peso);
    }
    
    /**
     * Método que vai buscar um local associado a um nome 
     * @param nome Nome do local a ir buscar
     * @return Local pretendido
     */
    public Local getLocalOfNome(String nome) {
        Local localOfNome = null;
        for (Local l : mapaEstradas.vertices()) {
            if (l.nome.equals(nome)) {
                localOfNome = l;
            }
        }
        return localOfNome;
    }

    /**
     * Método que usa o caminhoMenorDificuldade e o devolve como uma lista de nomes.
     * @param localOrigem Vertice de origem para o menor caminho
     * @param localDestino Vertice de destino para o menor caminho
     * @return Lista de nomes de locais que representam o menor caminho
     */
    public LinkedList<String> caminhoMenorDificuldadeToString(Local localOrigem, Local localDestino) {

        LinkedList<Local> min = caminhoMenorDificuldade(localOrigem, localDestino);

        if (min.isEmpty()) {
            return null;
        }

        LinkedList<String> nomesLocais = new LinkedList<>();
        for (Local l : min) {
            nomesLocais.add(l.nome);
        }
        return nomesLocais;
    }
    
    /**
     * Método que usa o shortestPah para ir buscar o caminho de menor dificuldade, contando apenas com as dificuldades das estradas.
     * @param localOrigem Local onde se inicia
     * @param localDestino Local a que se quer chegar
     * @return Lista ligada dos locais a percorrer até chegar ao destino
     */
    public LinkedList<Local> caminhoMenorDificuldade(Local localOrigem, Local localDestino) {
        LinkedList<Local> caminho = new LinkedList<>();

        if (!mapaEstradas.checkVertex(localOrigem) || !mapaEstradas.checkVertex(localDestino) || localOrigem.equals(localDestino)) {
            return caminho;
        }

        double min = EdgeAsDoubleGraphAlgorithms.shortestPath(mapaEstradas, localOrigem, localDestino, caminho);

        if (min <= 0 || caminho.isEmpty()) {
            return caminho;
        }

        return caminho;
    }

   /**
     * Determina se um personagem pode conquistar um determinado local
     * Primeiro criar uma linkedlist com todos os caminhos  até ao local recebido por parâmetro a partir de todos locais pertencentes à personagem
     * De seguida, elimina dessa linkedlist os caminhos que a personagem não pode percorrer por falta de força
     * Verifica se a personagem consegue percorrer algum caminho e chama o método auxiliar
     * @param personagem
     * @param local
     * @param caminhoFinal
     * @return se não houver caminhos válidos, devolve 0, se for possível percorrer caminhos, devolve a força necessária para percorrer o mais favorável, 
     * se houver caminhos válidos mas a personagem não os conseguir percorrer, devolve o negativo do valor da força necessária para percorrer o que seria mais favorável
     */
    public int personagemPodeConquistarLocal(Personagem personagem, Local local, LinkedList<Local> caminhoFinal) {
        
        if(mapaEstradas == null || local == null || personagem == null) return 0;
        if(!mapaEstradas.checkVertex(local)) return 0;
        if(personagem.getLocais().contains(local)) return 0;
        if(caminhoFinal != null) caminhoFinal.clear();
        if(caminhoFinal == null) caminhoFinal = new LinkedList<>();
        
        LinkedList<LinkedList<Local>> todosOsCaminhosEntrePersonagemELocal = new LinkedList<>();

        for(Local localP : personagem.getLocais()) {
            GraphAlgorithms.allPaths(mapaEstradas, localP, local, todosOsCaminhosEntrePersonagemELocal);
        }
        
        if(todosOsCaminhosEntrePersonagemELocal.isEmpty()) return 0;
        LinkedList<LinkedList<Local>> listaDeCaminhosClone = (LinkedList) todosOsCaminhosEntrePersonagemELocal.clone();
        LinkedList<LinkedList<Local>> listaDeCaminhosAEliminar = new LinkedList<>();
        
        // remove os caminhos que não são  percorríveis por falta de força
        for(LinkedList<Local> caminho : todosOsCaminhosEntrePersonagemELocal) {
            if(!caminhoEPercorrivelSemAliado(caminho, personagem)) listaDeCaminhosAEliminar.add(caminho);
        }
        todosOsCaminhosEntrePersonagemELocal.removeAll(listaDeCaminhosAEliminar);
        
        // verifica se ainda há caminhos
        if(!todosOsCaminhosEntrePersonagemELocal.isEmpty()) { 
            
            return personagemPodeConquistarLocalAuxiliar(caminhoFinal, todosOsCaminhosEntrePersonagemELocal);
            
        } else {
            
            // dá return ao valor da força necessária para percorrer o caminho mais favorável, em negativo
            return -personagemPodeConquistarLocalAuxiliar(caminhoFinal, listaDeCaminhosClone);
            
        }
    }
    
    /**
     * Determina de uma lista de caminhos, qual o mais favorável
     * Primeiro escolhe os mais curtos em termos de número de Locais
     * Depois, dos restantes, escolhe qual o que requer menos força para ser percorrido
     * @param caminhoFinal
     * @param todosOsCaminhosEntrePersonagemELocal
     * @return valor da força necessária para percorrer o caminho mais favorável
     */
    private int personagemPodeConquistarLocalAuxiliar(LinkedList<Local> caminhoFinal, LinkedList<LinkedList<Local>> todosOsCaminhosEntrePersonagemELocal) {
        int size = Integer.MAX_VALUE;
        LinkedList<LinkedList<Local>> caminhosMaisCurtos = new LinkedList<>();

        // verifica quais os caminhos mais curto dos restantes  (em termos de número de locais e não em quantidade de força necessária)
        for(LinkedList<Local> caminho : todosOsCaminhosEntrePersonagemELocal) {
            if(caminho.size() < size) {
                caminhosMaisCurtos.clear();
                caminhosMaisCurtos.add(caminho);
                size = caminho.size();
            } else if (caminho.size() == size) {
                caminhosMaisCurtos.add(caminho);
            }
        }

        // dos caminhos restantes verifica qual o que tem menor requerimento de força para ser percorrido
        size = Integer.MAX_VALUE;
        int forcaNecessaria;
        for(LinkedList<Local> caminho : caminhosMaisCurtos) {
            forcaNecessaria = forcaNecessariaParaPercorrerCaminho(caminho);
            if(forcaNecessaria < size) {
                caminhoFinal.clear();
                caminhoFinal.addAll(caminho);
                size = forcaNecessaria;
            }
        }

        // determina a força necessária para percorrer o caminho percorrível mais curto em termos de número de locais
        return size;
    }
    
    /**
     * Verifica se um determinado caminho é percorrível por uma personagem, sem ajuda da aliança
     * @param caminho
     * @param personagem
     * @return true se a personagem pode percorrer o caminho, false se não pode
     */
    public boolean caminhoEPercorrivelSemAliado(LinkedList<Local> caminho, Personagem personagem) {
        if(caminho == null || personagem == null) return false;
        if(caminho.isEmpty()) return false;
        
        int forcaNecessaria = forcaNecessariaParaPercorrerCaminho(caminho);
        
        if(forcaNecessaria == -1) return false;
        if(forcaNecessaria > personagem.getForca()) return false;
        
        return true;
    }
    
    /**
     * Determina o valor de força necessária para percorrer um determinado caminho
     * @param caminho
     * @return 
     */
    public int forcaNecessariaParaPercorrerCaminho(LinkedList<Local> caminho) {
        if(caminho == null) return -1;
        if(caminho.isEmpty()) return -1;
        
        Iterator<Local> it = caminho.iterator();
        Iterator<Local> it2 = caminho.iterator();
        it2.next();
        int forcaNecessaria, forcaMaxima = 0;
        
        do {
            Local local1 = it.next();
            Local local2 = it2.next();
            if(mapaEstradas.getEdge(local1, local2) == null) return -1;
            forcaNecessaria = local2.getPontos() + mapaEstradas.getEdge(local1, local2).intValue();
            if(local2.getPersonagem() != null) {
                forcaMaxima = forcaMaxima + forcaNecessaria + local2.getPersonagem().getForca();
            } else {
                forcaMaxima = forcaMaxima + forcaNecessaria;
            }
        } while(it2.hasNext());
        
        return forcaMaxima;
    }
}

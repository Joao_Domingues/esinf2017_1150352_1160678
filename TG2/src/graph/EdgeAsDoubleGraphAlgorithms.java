
package graph;

import java.util.LinkedList;

/**
 *
 * @author DEI-ESINF
 */
public class EdgeAsDoubleGraphAlgorithms {

    /**
     * Determine the shortest path to all vertices from a vertex using Dijkstra's algorithm
     * To be called by public short method
     *
     * @param graph Graph object
     * @param sourceIdx Source vertex 
     * @param knownVertices previously discovered vertices
     * @param verticesIndex index of vertices in the minimum path
     * @param minDist minimum distances in the path
     *
     */
    private static <V> void shortestPath(AdjacencyMatrixGraph<V,Double> graph, int sourceIdx, boolean[] knownVertices, int[] verticesIndex, double [] minDist)	{ 
        for(V vertex : graph.vertices){
            int index = graph.toIndex(vertex);
            minDist[index] = Double.POSITIVE_INFINITY;
            verticesIndex[index] = -1;
            knownVertices[index] = false;
        }
        minDist[sourceIdx] = 0;
        Double edge;
        while(sourceIdx != -1){
            knownVertices[sourceIdx] = true;
            V sourceVertex = graph.vertices.get(sourceIdx);
            for(V vAdj : graph.directConnections(sourceVertex)){
                edge = graph.getEdge(vAdj, sourceVertex);
                int index = graph.toIndex(vAdj);
                if(knownVertices[index] == false && minDist[index] > (minDist[sourceIdx] + edge)){
                    minDist[index] = minDist[sourceIdx] + edge;
                    verticesIndex[index] = sourceIdx;
                }
            }
            sourceIdx = minDistGraph(minDist, knownVertices);
        }
    }


    /**
     * Determine the shortest path between two vertices using Dijkstra's algorithm
     *
     * @param graph Graph object
     * @param source Source vertex 
     * @param dest Destination vertices
     * @param path Returns the vertices in the path (empty if no path)
     * @return minimum distance, -1 if vertices not in graph or no path
     *
     */
    public static <V> double shortestPath(AdjacencyMatrixGraph<V, Double> graph, V source, V dest, LinkedList<V> path){
        int sourceIdx = graph.toIndex(source);
        int destIdx = graph.toIndex(dest);
        path.clear();
        
        if(sourceIdx == -1 || destIdx == -1){
            return -1;
        }
        
        
        boolean[] knownVertices = new boolean[graph.numVertices];
        int[] verticesIndex = new int[graph.numVertices];
        double[] minDist = new double[graph.numVertices];
        
        shortestPath(graph, sourceIdx, knownVertices, verticesIndex, minDist);
        
        if(knownVertices[destIdx] == false){
            return -1;
        }
        
        recreatePath(graph, sourceIdx, destIdx, verticesIndex, path);
        
         if(source.equals(dest)){
            return 0;
        }
         
        //Reverter a ordem, usando uma stack auxiliar
        LinkedList<V> stack = new LinkedList<>();
        while(!path.isEmpty()){
            stack.push(path.remove());
        }
        while(!stack.isEmpty()){
            path.add(stack.pop());
        }
        
        Double dist = minDist[destIdx];
        return dist;
    }


    /**
     * Recreates the minimum path between two vertex, from the result of Dikstra's algorithm
     * 
     * @param graph Graph object
     * @param sourceIdx Source vertex 
     * @param destIdx Destination vertices
     * @param verticesIndex index of vertices in the minimum path
     * @param Queue Vertices in the path (empty if no path)
     */
    private static <V> void recreatePath(AdjacencyMatrixGraph<V, Double> graph, int sourceIdx, int destIdx, int[] verticesIndex, LinkedList<V> path){

        path.add(graph.vertices.get(destIdx));
        if (sourceIdx != destIdx){
            destIdx = verticesIndex[destIdx];        
            recreatePath(graph, sourceIdx, destIdx, verticesIndex, path);
        }
    }

    /**
     * Creates new graph with minimum distances between all pairs
     * uses the Floyd-Warshall algorithm
     * 
     * @param <V>
     * @param minDist
     * @param knownVertices
     * @return the new graph 
     */

    public static <V> int minDistGraph(double[] minDist, boolean[] knownVertices){
        int sourceIdx = -1;
        Double min = Double.POSITIVE_INFINITY;
        
        for(int i = 0; i < knownVertices.length; i++){
            if(!knownVertices[i] && minDist[i] < min){
                min = minDist[i];
                sourceIdx = i;
            }
        }
        return sourceIdx;
    }


}

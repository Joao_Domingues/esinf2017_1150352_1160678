package graph;

import java.util.Iterator;
import java.util.LinkedList;

/**
 *
 * @author HP
 */
public class LabyrinthCheater {
    
    private AdjacencyMatrixGraph<Room, Door> map;
    
    public LabyrinthCheater(){
        map = new AdjacencyMatrixGraph<>();
    }
    
    /**
     * Inner class para a sala, definimos para as duas variaveis usadas no grafo.
     */
    private class Room{
        public String name;
        public boolean hasExit;
        
        public Room(String n, boolean exit){
            name = n;
            hasExit = exit;
        }
        
        /**
         * necessario reescrever.
         * @param other
         * @return 
         */
        @Override
        public boolean equals(Object other){
            if(!(other instanceof Room)) return false;
            return name.equalsIgnoreCase(((Room)other).name);
        }
    }
    
    private class Door{
    }
    
    /**
     * Returns a list of rooms which are reachable from one room
     * @param room Nome do quarto
     * @return LinkedList com o nome dos quartos encontrados com o algoritmo DFS.
     */
    public Iterable<String> roomsInReach(String room){
        if(!map.checkVertex(new Room(room, false))) return null;
        
        LinkedList<String> names = new LinkedList<>();
        
        LinkedList<Room> rooms = GraphAlgorithms.DFS(map, new Room(room, false));
        
        for(Room r : rooms){
            names.add(r.name);
        }
        
        return names;
    }
    
    /**
     * Checks which rooms has the nearest exit.
     * @param roomName Name of the room to start looking from
     * @return Name of the room with the nearest exit.
     */
    public String nearestRoomWithExit(String roomName){
        if(!map.checkVertex(new Room(roomName, false))) return null;
        
        LinkedList<Room> rooms = GraphAlgorithms.BFS(map, new Room(roomName, false));
        
        for(Room r : rooms){
            if(r.hasExit) return r.name;
        }
        return null;
    }
    
    public LinkedList<String> pathToString(String from){
        if(!map.checkVertex(new Room(from, false))) return null;
        
        String exitName = nearestRoomWithExit(from); //determinar a saida mais proxima
        if(exitName == null) return null;
        LinkedList<String> names = new LinkedList<>();
        if(exitName.equalsIgnoreCase(from)){
            names.add(exitName);
            return names;
        }
        LinkedList<LinkedList<Room>> paths = new LinkedList<>();
        boolean result = GraphAlgorithms.allPaths(map, new Room(from, false), new Room(exitName, true), paths); //all paths para um vertice e a sua saida mais proxima
        
        if(result == false || paths.isEmpty()) return null;
        
        Iterator<LinkedList<Room>> it = paths.iterator();
        LinkedList<Room> min = it.next();
        while(it.hasNext()){
            LinkedList<Room> current = it.next();
            if(current.size() < min.size()) min = current;
        }
        for(Room r : min){
            names.add(r.name);
        }
        return names;
    }
}
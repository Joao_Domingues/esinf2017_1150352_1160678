/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esinf_1150352_1160678;

import graph.AdjacencyMatrixGraph;
import java.util.LinkedList;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author HP
 */
public class MapaEstradasTest {

    MapaEstradas mapaEstradasTeste = new MapaEstradas();

    Personagem p1 = new Personagem("p1", 3);

    Personagem p2 = new Personagem("p2", 100);

    Local l1 = new Local("Local01", p1, 12);
    Local l2 = new Local("Local02", p1, 30);
    Local l3 = new Local("Local03", p1, 40);
    Local l4 = new Local("Local04", p1, 29);
    Local l5 = new Local("Local05", p1, 1);
    Local l6 = new Local("Local06", p1, 85);
    Local l7 = new Local("Local07", p1, 239);
    Local l8 = new Local("Local08", p1, 23);
    Local l9 = new Local("Local09", p1, 14);

    Local l10 = new Local("Local10", p2, 15 );
    Local l11 = new Local("Local11", p2, 13);
    Local l12 = new Local("Local12", null, 5);
    Local l13 = new Local("Local13", null, 75);
    
    public MapaEstradasTest() {
    }

    @Before
    public void setUp() throws Exception {

        mapaEstradasTeste.inserirLocal(l1);
        mapaEstradasTeste.inserirLocal(l2);
        mapaEstradasTeste.inserirLocal(l3);
        mapaEstradasTeste.inserirLocal(l4);
        mapaEstradasTeste.inserirLocal(l5);
        mapaEstradasTeste.inserirLocal(l6);
        mapaEstradasTeste.inserirLocal(l7);
        mapaEstradasTeste.inserirLocal(l8);
        mapaEstradasTeste.inserirLocal(l9);

        mapaEstradasTeste.inserirLocal(l10);
        mapaEstradasTeste.inserirLocal(l11);
        mapaEstradasTeste.inserirLocal(l12);
        mapaEstradasTeste.inserirLocal(l13);
        
        mapaEstradasTeste.inserirCaminho(l1.nome, l2.nome, 4.0);
        mapaEstradasTeste.inserirCaminho(l1.nome, l4.nome, 5.0);
        mapaEstradasTeste.inserirCaminho(l2.nome, l6.nome, 2.0);
        mapaEstradasTeste.inserirCaminho(l2.nome, l3.nome, 8.0);
        mapaEstradasTeste.inserirCaminho(l3.nome, l9.nome, 2.0);
        mapaEstradasTeste.inserirCaminho(l3.nome, l7.nome, 1.0);
        mapaEstradasTeste.inserirCaminho(l3.nome, l8.nome, 7.0);
        mapaEstradasTeste.inserirCaminho(l9.nome, l8.nome, 3.0);
        mapaEstradasTeste.inserirCaminho(l5.nome, l8.nome, 6.0);
        mapaEstradasTeste.inserirCaminho(l7.nome, l4.nome, 1.0);
        mapaEstradasTeste.inserirCaminho(l4.nome, l5.nome, 10.0);

        mapaEstradasTeste.inserirCaminho(l7.nome, l10.nome, 5.0);
        mapaEstradasTeste.inserirCaminho(l4.nome, l5.nome, 7.0);
        mapaEstradasTeste.inserirCaminho(l10.nome, l13.nome, 12.0);
        mapaEstradasTeste.inserirCaminho(l10.nome, l3.nome, 3.0);
        mapaEstradasTeste.inserirCaminho(l11.nome, l12.nome, 9.0);
        mapaEstradasTeste.inserirCaminho(l12.nome, l1.nome, 6.0);
        mapaEstradasTeste.inserirCaminho(l13.nome, l2.nome, 6.0);
        mapaEstradasTeste.inserirCaminho(l13.nome, l6.nome, 1.0);
        
        p1.addLocal(l1);
        p1.addLocal(l2);
        p1.addLocal(l3);
        p1.addLocal(l4);
        p1.addLocal(l5);
        p1.addLocal(l6);
        p1.addLocal(l7);
        p1.addLocal(l8);
        p1.addLocal(l9);

        p2.addLocal(l10);
        p2.addLocal(l11);

    }

    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of inserirLocal method, of class MapaEstradas.
     */
    @Test
    public void testInserirLocal() {
        System.out.println("inserirLocal");
        Local l = null;
        boolean expResult = false;
        boolean result = mapaEstradasTeste.inserirLocal(l);
        assertEquals(expResult, result);

        Local l20 = new Local("Local20", 2);
        boolean result1 = mapaEstradasTeste.inserirLocal(l20);
        assertEquals(true, result1);
    }

    /**
     * Test of inserirCaminho method, of class MapaEstradas.
     */
    @Test
    public void testInserirCaminho() { //este método já testa o getLocalOfNome
        System.out.println("inserirCaminho");
        String l10 = null;
        String l20 = null;
        double peso = -1;
        boolean expResult = false;
        boolean result = mapaEstradasTeste.inserirCaminho(l10, l20, peso);
        assertEquals(expResult, result);

        boolean result1 = mapaEstradasTeste.inserirCaminho("Local01", l20, peso);
        boolean result2 = mapaEstradasTeste.inserirCaminho(l10, "Local02", peso);
        boolean result3 = mapaEstradasTeste.inserirCaminho(l10, l20, 4);
        boolean result4 = mapaEstradasTeste.inserirCaminho("Local01", "Local03", 12.0);

        assertEquals(false, result1);
        assertEquals(false, result2);
        assertEquals(false, result3);
        assertEquals(true, result4);
    }

    /**
     * Test of caminhoMenorDificuldadeToString method, of class MapaEstradas.
     */
    @Test
    public void testCaminhoMenorDificuldadeToString() { //Testa o método caminhoMenorDificuldade também
        System.out.println("caminhoMenorDificuldadeToString");
        Local localInexistente1 = new Local("Local30", 32);

        LinkedList<String> listaNull = new LinkedList<>();

        listaNull = mapaEstradasTeste.caminhoMenorDificuldadeToString(localInexistente1, l1);
        assertEquals(null, listaNull);

        listaNull = mapaEstradasTeste.caminhoMenorDificuldadeToString(l1, localInexistente1);
        assertEquals(null, listaNull);

        listaNull = mapaEstradasTeste.caminhoMenorDificuldadeToString(l1, l1);
        assertEquals(null, listaNull);

        listaNull = mapaEstradasTeste.caminhoMenorDificuldadeToString(l1, l8);
        LinkedList<String> expResult = new LinkedList<>();
        expResult.add("Local01");
        expResult.add("Local04");
        expResult.add("Local07");
        expResult.add("Local03");
        expResult.add("Local09");
        expResult.add("Local08");

        assertEquals(expResult, listaNull);
        assertTrue("Teste do size da lista do caminho mais curto.", listaNull.size() == 6);

        expResult.clear();
        listaNull.clear();

        listaNull = mapaEstradasTeste.caminhoMenorDificuldadeToString(l6, l9);
        expResult.add("Local06");
        expResult.add("Local02");
        expResult.add("Local03");
        expResult.add("Local09");

        assertEquals(expResult, listaNull);
        assertTrue("Teste para o size", listaNull.size() == 4);
    }

    /**
     * Test of personagemPodeConquistarLocal method, of class MapaEstradas.
     */
    @Test
    public void testPersonagemPodeConquistarLocal() {
        System.out.println("personagemPodeConquistarLocal");

        Personagem personagem = null;
        Local local1 = null;
        LinkedList<Local> locais = null;
        MapaEstradas instance = new MapaEstradas();
        // teste com os valores a null
        int expResult = 0;
        int result = instance.personagemPodeConquistarLocal(personagem, local1, locais);
        assertEquals(expResult, result);

        locais = new LinkedList<>();

        personagem = new Personagem("teste", 25);

        // teste com um local que não existe no mapaEstradas
        local1 = new Local("teste", null, 10);
        result = instance.personagemPodeConquistarLocal(personagem, local1, locais);
        assertEquals(expResult, result);
        assertEquals(0, locais.size());
        

        // teste com apenas dois locais, em que a personagem é dona de um deles, mas não há caminhos que liguem um ao outro
        local1 = new Local("Local01", null, 12);
        Local local2 = new Local("Local02", personagem, 30);
        instance.inserirLocal(local1);
        instance.inserirLocal(local2);
        personagem.addLocal(local2);
        result = instance.personagemPodeConquistarLocal(personagem, local1, locais);
        assertEquals(expResult, result);
        assertEquals(0, locais.size());
        

        // teste com apenas dois locais, que estão ligados, mas a personagem não é dona de nenhum
        instance.getMapaEstradas().removeVertex(local2);
        personagem.removeLocal(local2);
        local2 = new Local("Local02", null, 30);
        instance.inserirLocal(local2);
        instance.inserirCaminho(local1.getNome(), local2.getNome(), 10.0);
        result = instance.personagemPodeConquistarLocal(personagem, local1, locais);
        assertEquals(expResult, result);
        assertEquals(0, locais.size());
        

        // teste com apenas dois locais, ligados, em que a personagem é dona dos dois
        instance.getMapaEstradas().removeVertex(local1);
        instance.getMapaEstradas().removeVertex(local2);
        personagem.removeLocal(local2);
        local1 = new Local("Local01", personagem, 30);
        local2 = new Local("Local02", personagem, 30);
        personagem.addLocal(local1);
        personagem.addLocal(local2);
        instance.inserirLocal(local1);
        instance.inserirLocal(local2);
        instance.inserirCaminho(local1.getNome(), local2.getNome(), 10.0);
        result = instance.personagemPodeConquistarLocal(personagem, local1, locais);
        assertEquals(expResult, result);
        assertEquals(0, locais.size());
        

        // teste com apenas dois locais, ligados, em que a personagem é dona de um só, mas não tem força para conquistar o outro
        instance.getMapaEstradas().removeVertex(local2);
        personagem.removeLocal(local2);
        local2 = new Local("Local02", null, 30);
        instance.inserirLocal(local2);
        instance.inserirCaminho(local1.getNome(), local2.getNome(), 10.0);
        expResult = -40;
        result = instance.personagemPodeConquistarLocal(personagem, local2, locais);
        assertEquals(expResult, result);
        assertEquals(2, locais.size());
        

        // teste com apenas dois locais, ligados, em que a personagem é dona de um só, e tem força para conquistar o outro
        locais.clear();
        instance.getMapaEstradas().removeVertex(local2);
        personagem.removeLocal(local2);
        local2 = new Local("Local02", null, 10);
        instance.inserirLocal(local2);
        instance.inserirCaminho(local1.getNome(), local2.getNome(), 10.0);
        expResult = 20;
        result = instance.personagemPodeConquistarLocal(personagem, local2, locais);
        assertEquals(expResult, result);
        assertEquals(2, locais.size());
        

        // teste com múltiplos locais, sem estarem todos ligados, em que a personagem é dona de dois, um deles que não é ligado
        locais.clear();
        Local local3 = new Local("Local03", personagem, 10);
        instance.inserirLocal(local3);
        personagem.addLocal(local3);
        result = instance.personagemPodeConquistarLocal(personagem, local2, locais);
        assertEquals(expResult, result);
        assertEquals(2, locais.size());
        

        // teste com múltiplos locais, todos ligados entre si, em que a personagem é dona de dois deles
        locais.clear();
        instance.inserirCaminho(local2.getNome(), local3.getNome(), 20.0);
        instance.inserirCaminho(local1.getNome(), local3.getNome(), 1.0);
        result = instance.personagemPodeConquistarLocal(personagem, local2, locais);
        assertEquals(expResult, result);
        assertEquals(2, locais.size());
        

        // teste com múltiplos locais, todos ligados entre si, em que a personagem é dona de dois deles, mas não consegue conquistar o terceiro
        locais.clear();
        instance.getMapaEstradas().removeEdge(local1, local2);
        instance.inserirCaminho(local1.getNome(), local2.getNome(), 40.0);
        result = instance.personagemPodeConquistarLocal(personagem, local2, locais);
        expResult = -30;
        assertEquals(expResult, result);
        assertEquals(2, locais.size());
        

        // teste com uma amostra maior, em que a personagem tenta conquistar um Local sem dono para o qual tem força necessária
        locais.clear();
        result = mapaEstradasTeste.personagemPodeConquistarLocal(p2, l12, locais);
        expResult = 14;
        assertEquals(expResult, result);
        assertEquals(2, locais.size());
        

        // teste com uma amostra maior, em que a personagem tenta conquistar um Local sem dono para o qual não tem força necessária
        locais.clear();
        result = mapaEstradasTeste.personagemPodeConquistarLocal(p1, l13, locais);
        expResult = -76;
        assertEquals(expResult, result);
        assertEquals(2, locais.size());
        

        // teste com uma amostra maior, em que a personagem tenta conquistar um Local com dono para o qual não tem força necessária
        locais.clear();
        result = mapaEstradasTeste.personagemPodeConquistarLocal(p1, l11, locais);
        expResult = -133;
        assertEquals(expResult, result);
        assertEquals(3, locais.size());
        

        // teste com uma amostra maior, em que a personagem tenta conquistar um Local com dono para o qual tem força necessária
        locais.clear();
        result = mapaEstradasTeste.personagemPodeConquistarLocal(p2, l1, locais);
        expResult = 35;
        assertEquals(expResult, result);
        assertEquals(3, locais.size());
        

        // teste com uma amostra maior, em que a personagem tenta conquistar um Local com dono para o qual tem força necessária
        locais.clear();
        result = mapaEstradasTeste.personagemPodeConquistarLocal(p2, l2, locais);
        expResult = 87;
        assertEquals(expResult, result);
        assertEquals(3, locais.size());
        

    }

    /**
     * Test of caminhoEPercorrivelSemAliado method, of class MapaEstradas.
     */
    @org.junit.Test
    public void testCaminhoEPercorrivelSemAliado() {
        System.out.println("caminhoEPercorrivelSemAliado");

        LinkedList<Local> caminho = null;
        Personagem personagem = null;

        // teste com valores nulos
        boolean expResult = false;
        boolean result = mapaEstradasTeste.caminhoEPercorrivelSemAliado(caminho, personagem);
        assertEquals(expResult, result);

        // teste com caminho vazio
        caminho = new LinkedList<>();
        result = mapaEstradasTeste.caminhoEPercorrivelSemAliado(caminho, personagem);
        assertEquals(expResult, result);

        // teste com caminho inválido (vértices não estão todos ligados)
        caminho.add(l11);
        caminho.add(l12);
        caminho.add(l2);
        result = mapaEstradasTeste.caminhoEPercorrivelSemAliado(caminho, p2);
        assertEquals(expResult, result);

        // teste com caminho inválido (vértices não pertencem ao mapa)
        Local teste = new Local("teste", null, 10);
        caminho.remove(l2);
        caminho.add(teste);
        result = mapaEstradasTeste.caminhoEPercorrivelSemAliado(caminho, p2);
        assertEquals(expResult, result);

        // teste com caminho válido, não percorrível pela personagem
        caminho.remove(teste);
        caminho.add(l1);
        result = mapaEstradasTeste.caminhoEPercorrivelSemAliado(caminho, p1);
        assertEquals(expResult, result);

        // teste com caminho válido, percorrível pela personagem
        expResult = true;
        result = mapaEstradasTeste.caminhoEPercorrivelSemAliado(caminho, p2);
        assertEquals(expResult, result);
    }

    /**
     * Test of forcaNecessariaParaPercorrerCaminho method, of class
     * MapaEstradas.
     */
    @org.junit.Test
    public void testForcaNecessariaParaPercorrerCaminho() {
        System.out.println("forcaNecessariaParaPercorrerCaminho");

        LinkedList<Local> caminho = null;

        // teste com valores nulos
        int expResult = -1;
        int result = mapaEstradasTeste.forcaNecessariaParaPercorrerCaminho(caminho);
        assertEquals(expResult, result);

        // teste com caminho vazio
        caminho = new LinkedList<>();
        result = mapaEstradasTeste.forcaNecessariaParaPercorrerCaminho(caminho);
        assertEquals(expResult, result);

        // teste com caminho inválido (vértices não estão todos ligados)
        caminho.add(l11);
        caminho.add(l12);
        caminho.add(l2);
        result = mapaEstradasTeste.forcaNecessariaParaPercorrerCaminho(caminho);
        assertEquals(expResult, result);

        // teste com caminho inválido (vértices não pertencem ao mapa)
        Local teste = new Local("teste", null, 10);
        caminho.remove(l2);
        caminho.add(teste);
        result = mapaEstradasTeste.forcaNecessariaParaPercorrerCaminho(caminho);
        assertEquals(expResult, result);

        // teste com caminho válido
        caminho.remove(teste);
        caminho.add(l1);
        expResult = 35;
        result = mapaEstradasTeste.forcaNecessariaParaPercorrerCaminho(caminho);
        assertEquals(expResult, result);
    }

}

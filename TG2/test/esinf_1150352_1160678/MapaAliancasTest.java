/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esinf_1150352_1160678;

import graphbase.Edge;
import graphbase.Graph;
import java.util.Iterator;
import java.util.LinkedList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.BeforeClass;

/**
 *
 * @author HP
 */
public class MapaAliancasTest {

    MapaAliancas mapaAliancasTeste = new MapaAliancas();

    Personagem p0 = new Personagem("Pers0", 195);
    Personagem p1 = new Personagem("Pers1", 111);
    Personagem p2 = new Personagem("Pers2", 112);
    Personagem p3 = new Personagem("Pers3", 429);
    Personagem p4 = new Personagem("Pers4", 262);
    Personagem p5 = new Personagem("Pers5", 126);
    Personagem p6 = new Personagem("Pers6", 121);
    Personagem p7 = new Personagem("Pers7", 354);
    Personagem p8 = new Personagem("Pers8", 481);
    Personagem p9 = new Personagem("Pers9", 463);

    public MapaAliancasTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        p0.addLocal(new Local("Local2", 10));
        p1.addLocal(new Local("Local4", 10));
        p2.addLocal(new Local("Local6", 10));
        p3.addLocal(new Local("Local8", 10));
        p4.addLocal(new Local("Local10", 10));
        p5.addLocal(new Local("Local12", 10));
        p6.addLocal(new Local("Local14", 10));
        p7.addLocal(new Local("Local16", 10));
        p8.addLocal(new Local("Local18", 10));
        p9.addLocal(new Local("Local20", 10));

        mapaAliancasTeste.inserirPersonagem(p0);
        mapaAliancasTeste.inserirPersonagem(p1);
        mapaAliancasTeste.inserirPersonagem(p2);
        mapaAliancasTeste.inserirPersonagem(p3);
        mapaAliancasTeste.inserirPersonagem(p4);
        mapaAliancasTeste.inserirPersonagem(p5);
        mapaAliancasTeste.inserirPersonagem(p6);
        mapaAliancasTeste.inserirPersonagem(p7);
        mapaAliancasTeste.inserirPersonagem(p8);
        mapaAliancasTeste.inserirPersonagem(p9);

        mapaAliancasTeste.inserirAlianca(p0.nome, p3.nome, true, 0.8);
        mapaAliancasTeste.inserirAlianca(p0.nome, p4.nome, true, 0.5);
        mapaAliancasTeste.inserirAlianca(p3.nome, p1.nome, true, 0.5);
        mapaAliancasTeste.inserirAlianca(p4.nome, p1.nome, true, 0.2);
        mapaAliancasTeste.inserirAlianca(p1.nome, p2.nome, true, 0.5);
        mapaAliancasTeste.inserirAlianca(p5.nome, p7.nome, false, 0.5);
        mapaAliancasTeste.inserirAlianca(p7.nome, p9.nome, true, 0.5);
        mapaAliancasTeste.inserirAlianca(p7.nome, p8.nome, true, 0.5);
        mapaAliancasTeste.inserirAlianca(p6.nome, p8.nome, true, 0.8);
    }

    @After
    public void tearDown() throws Exception {
    }

    /**
     * Test of aliados method, of class MapaAliancas.
     */
    @Test
    public void testAliados() {
        System.out.println("aliados");
        Personagem p = null;
        LinkedList<Personagem> expResult = null;
        LinkedList<Personagem> result = mapaAliancasTeste.aliados(p);
        assertEquals(expResult, result);
        
        p = new Personagem("Pers11", 132);
        result = mapaAliancasTeste.aliados(p);
        assertEquals(expResult, result);
        
        Personagem p10 = new Personagem("Pers10", 321);
        mapaAliancasTeste.inserirPersonagem(p10);
        result = mapaAliancasTeste.aliados(p10);
        assertEquals(expResult, result);
        
        result = mapaAliancasTeste.aliados(p0);
        expResult = new LinkedList<>();
        expResult.add(p3);
        expResult.add(p4);
        assertEquals(expResult, result);
        
        expResult.clear();
        result.clear();
        
        expResult.add(p5);
        expResult.add(p9);
        expResult.add(p8);
        result = mapaAliancasTeste.aliados(p7);
        assertEquals(expResult, result);
    }

    /**
     * Test of aliancaMaisForte method, of class MapaAliancas.
     */
    @Test
    public void testAliancaMaisForte() {
        Personagem p10 = new Personagem("Pers10", 195);
        Personagem p11 = new Personagem("Pers11", 429);
        mapaAliancasTeste.inserirPersonagem(p10);
        mapaAliancasTeste.inserirPersonagem(p11);
        
        System.out.println("aliancaMaisForte");
        LinkedList<Alianca> expResult = new LinkedList<>();
        expResult.add(new Alianca(p0, p3, true, 0.8));
        expResult.add(new Alianca(p10, p11, true, 0.8));
        mapaAliancasTeste.inserirAlianca(p10.nome, p11.nome, true, 0.8);
        LinkedList<Alianca> result = mapaAliancasTeste.aliancaMaisForte();
        Iterator<Alianca> expIt = expResult.iterator();
        Iterator<Alianca> it = result.iterator();
        Alianca expA = expIt.next();
        Alianca a = it.next();
        assertTrue("Teste para ver se são iguais", a.equals(expA));


        a = it.next();
        expA = expIt.next();
        assertTrue("Teste para ver se duas alianças com a mesma força estão na lista", a.equals(expA));
    }

    /**
     * Test of criarAlianca method, of class MapaAliancas.
     */
    @Test
    public void testCriarAlianca() { //Este teste ja testa o método getEdge e também o método removeEdge
        System.out.println("criarAlianca");
        Personagem a = null;
        Personagem b = null;
        boolean expResult = false;
        boolean result = mapaAliancasTeste.criarAlianca(a, b);
        assertEquals(expResult, result);
        
        result = mapaAliancasTeste.criarAlianca(p0, p3);
        assertTrue("Já são aliados: ", result == false);
        
        result = mapaAliancasTeste.criarAlianca(p0, p1);
        Edge<Personagem, Alianca> e = mapaAliancasTeste.getEdge(p0, p1);
        assertTrue("Não são aliados e é possivel chegar de um até ao outro", result == true);
        assertTrue("Compatibilidade é a média? ", e.getElement().getCompatibilidade() == 0.65);
        mapaAliancasTeste.removeEdge(p0, p1);

        
        result = mapaAliancasTeste.criarAlianca(p0, p5);
        e = mapaAliancasTeste.getEdge(p0, p5);
        System.out.println("Random: " + e.getElement().getCompatibilidade());
        assertTrue("Não são aliados e não estão na rede de alianças um do outro.", result == true);
        mapaAliancasTeste.removeEdge(p0, p5);
        
        
        result = mapaAliancasTeste.criarAlianca(p5, p6);
        e = mapaAliancasTeste.getEdge(p5, p6);
        assertTrue("Outro teste para se existir na rede de aliados: ", result == true);
        assertTrue("Compatibilidade média: ", e.getElement().getCompatibilidade() == 0.6);
        mapaAliancasTeste.removeEdge(p5, p6);
        
        Personagem p10 = new Personagem("Pers10", 100);
        mapaAliancasTeste.inserirPersonagem(p10);//Personagem sem alianças
        
        result = mapaAliancasTeste.criarAlianca(p10, p1);
        e = mapaAliancasTeste.getEdge(p10, p1);
        assertTrue("Aliança criada apesar de p10 não ter aliados: ", result == true);
        System.out.println("Random: " + e.getElement().getCompatibilidade());
        mapaAliancasTeste.removeEdge(p10, p1);
    }

    /**
     * Test of inserirPersonagem method, of class MapaAliancas.
     */
    @Test
    public void testInserirPersonagem() {
        System.out.println("inserirPersonagem");
        Personagem p = null;
        boolean expResult = false;
        boolean result = mapaAliancasTeste.inserirPersonagem(p);
        assertEquals(expResult, result);
        
        result = mapaAliancasTeste.inserirPersonagem(new Personagem("Pers20", 195));
        assertEquals(true, result);
    }

    /**
     * Test of inserirAlianca method, of class MapaAliancas.
     */
    @Test
    public void testInserirAlianca() { //também testa o getPersonagemOfNome
        System.out.println("inserirAlianca");
        String nomeP1 = null;
        String nomeP2 = null;
        boolean tipo = false;
        double compatibilidade = 0.0;
        boolean expResult = false;
        boolean result = mapaAliancasTeste.inserirAlianca(nomeP1, nomeP2, tipo, compatibilidade);
        assertEquals(expResult, result);
        
        result = mapaAliancasTeste.inserirAlianca("Pers10", "Pers0", true, 0.8);
        assertEquals(expResult, result);
        
        result = mapaAliancasTeste.inserirAlianca("Pers0", "Pers10", true, 0.5);
        assertEquals(expResult, result);
        
        result = mapaAliancasTeste.inserirAlianca("Pers0", "Pers1", true, -1.0);
        assertEquals(expResult, result);
        
        result = mapaAliancasTeste.inserirAlianca("Pers0", "Pers1", true, 1.5);
        assertEquals(expResult, result);
        
        result = mapaAliancasTeste.inserirAlianca("Pers0", "Pers1", true, 0.9);
        assertEquals(true, result);
        
        result = mapaAliancasTeste.inserirAlianca("Pers8", "Pers9", false, 0.35);
        assertEquals(true, result);
    }

    /**
     * Test of graphAllPossibleAliancas method, of class MapaAliancas.
     */
    @Test
    public void testGraphAllPossibleAliancas() {
        System.out.println("graphAllPossibleAliancas");
        Graph<Personagem, Alianca> expResult = null;
        MapaAliancas mapaTest = new MapaAliancas();
        Graph<Personagem, Alianca> result = mapaTest.graphAllPossibleAliancas();
        assertEquals(expResult, result);
        
        Personagem p10 = new Personagem("Pers10", 100);

        result = mapaAliancasTeste.graphAllPossibleAliancas();
        Graph<Personagem, Alianca> mapaAliancasTeste1 = new Graph<>(false);
        mapaAliancasTeste1.insertVertex(p0);
        mapaAliancasTeste1.insertVertex(p1);
        mapaAliancasTeste1.insertVertex(p2);
        mapaAliancasTeste1.insertVertex(p3);
        mapaAliancasTeste1.insertVertex(p4);
        mapaAliancasTeste1.insertVertex(p5);
        mapaAliancasTeste1.insertVertex(p6);
        mapaAliancasTeste1.insertVertex(p7);
        mapaAliancasTeste1.insertVertex(p8);
        mapaAliancasTeste1.insertVertex(p9);
        
        Alianca a1 = new Alianca(p0, p3, true, 0.5);
        Alianca a2 = new Alianca(p0, p4, true, 0.5);
        Alianca a3 = new Alianca(p3, p1, true, 0.5);
        Alianca a4 = new Alianca(p4, p1, true, 0.5);
        Alianca a5 = new Alianca(p1, p2, true, 0.5);
        Alianca a6 = new Alianca(p5, p7, false, 0.5);
        Alianca a7 = new Alianca(p7, p9, true, 0.5);
        Alianca a8 = new Alianca(p7, p8, true, 0.5);
        Alianca a9 = new Alianca(p6, p8, true, 0.5);
        Alianca a10 = new Alianca(p0, p2, true, 0.5);
        Alianca a11 = new Alianca(p0, p1, true, 0.5);
        Alianca a12 = new Alianca(p2, p3, true, 0.5);
        Alianca a13 = new Alianca(p2, p4, true, 0.5);
        Alianca a14 = new Alianca(p5, p9, true, 0.5);
        Alianca a15 = new Alianca(p5, p6, true, 0.5);
        Alianca a16 = new Alianca(p5, p8, true, 0.5);
        Alianca a17 = new Alianca(p7, p6, true, 0.5);
        Alianca a18 = new Alianca(p9, p6, true, 0.5);
        Alianca a19 = new Alianca(p9, p8, true, 0.5);
        Alianca a20 = new Alianca(p3, p4, true, 0.5);
        
        mapaAliancasTeste1.insertEdge(p0, p3, a1, 1);
        mapaAliancasTeste1.insertEdge(p0, p4, a2, 1);
        mapaAliancasTeste1.insertEdge(p3, p1, a3, 1);
        mapaAliancasTeste1.insertEdge(p4, p1, a4, 1);
        mapaAliancasTeste1.insertEdge(p1, p2, a5, 1);
        mapaAliancasTeste1.insertEdge(p3, p4, a20, 1);
        mapaAliancasTeste1.insertEdge(p5, p7, a6, 1);
        mapaAliancasTeste1.insertEdge(p7, p9, a7, 1);
        mapaAliancasTeste1.insertEdge(p7, p8, a8, 1);
        mapaAliancasTeste1.insertEdge(p6, p8, a9, 1);
        mapaAliancasTeste1.insertEdge(p0, p2, a10, 1);
        mapaAliancasTeste1.insertEdge(p0, p1, a11, 1);
        mapaAliancasTeste1.insertEdge(p2, p3, a12, 1);
        mapaAliancasTeste1.insertEdge(p2, p4, a13, 1);
        mapaAliancasTeste1.insertEdge(p5, p9, a14, 1);
        mapaAliancasTeste1.insertEdge(p5, p6, a15, 1);
        mapaAliancasTeste1.insertEdge(p5, p8, a16, 1);
        mapaAliancasTeste1.insertEdge(p7, p6, a17, 1);
        mapaAliancasTeste1.insertEdge(p9, p6, a18, 1);
        mapaAliancasTeste1.insertEdge(p9, p8, a19, 1);

        assertEquals(mapaAliancasTeste1, result); //verifica se as instancias são iguais(mapaAliancasTeste1 foi criado para ter todas as aliancas possiveis)
        
        assertTrue("Número de vertices", result.numVertices() == 10);
        
        assertTrue("Número de edges", result.numEdges() == 40);
        
        mapaAliancasTeste.inserirPersonagem(p10);
        mapaAliancasTeste1.insertVertex(p10);
        
        result = mapaAliancasTeste.graphAllPossibleAliancas();
        
        assertTrue("Vértice sem aliados", result.equals(mapaAliancasTeste1));
        
        assertTrue("Número de vértices", result.numVertices() == 11);
        
        assertTrue("Número de edges não se altera", result.numEdges() == 40);
    }
}

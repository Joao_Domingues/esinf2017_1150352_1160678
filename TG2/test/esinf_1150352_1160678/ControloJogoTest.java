/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esinf_1150352_1160678;

import java.util.LinkedList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Asus
 */
public class ControloJogoTest {
    
    ControloJogo controloJogoTest = new ControloJogo();
    MapaEstradas mapaEstradasTeste = controloJogoTest.getMapaEstradas();
    MapaAliancas mapaAliancasTeste = controloJogoTest.getMapaAliancas();
    
        Personagem p1 = new Personagem("p1", 50);
        Personagem p2 = new Personagem("p2", 100);
        Personagem p3 = new Personagem("p3", 10);
        Personagem p4 = new Personagem("p4", 30);
        
        Local l1 = new Local("Local01", p1, 12);
        Local l2 = new Local("Local02", p1, 30);
        Local l3 = new Local("Local03", p1, 40);
        Local l4 = new Local("Local04", p1, 29);
        Local l5 = new Local("Local05", p3, 1);
        Local l6 = new Local("Local06", p3, 85);
        Local l7 = new Local("Local07", p4, 239);
        Local l8 = new Local("Local08", p4, 23);
        Local l9 = new Local("Local09", null, 6);
        Local l10 = new Local("Local10", p2, 15);
        Local l11 = new Local("Local11", p2, 13);
        Local l12 = new Local("Local12", null, 300);
        Local l13 = new Local("Local13", null, 100);
    
    public ControloJogoTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() throws Exception {
        mapaEstradasTeste.inserirLocal(l1);
        mapaEstradasTeste.inserirLocal(l2);
        mapaEstradasTeste.inserirLocal(l3);
        mapaEstradasTeste.inserirLocal(l4);
        mapaEstradasTeste.inserirLocal(l5);
        mapaEstradasTeste.inserirLocal(l6);
        mapaEstradasTeste.inserirLocal(l7);
        mapaEstradasTeste.inserirLocal(l8);
        mapaEstradasTeste.inserirLocal(l9);
        mapaEstradasTeste.inserirLocal(l10);
        mapaEstradasTeste.inserirLocal(l11);
        mapaEstradasTeste.inserirLocal(l12);
        mapaEstradasTeste.inserirLocal(l13);

        mapaEstradasTeste.inserirCaminho(l1.nome, l2.nome, 4.0);
        mapaEstradasTeste.inserirCaminho(l1.nome, l4.nome, 5.0);
        mapaEstradasTeste.inserirCaminho(l2.nome, l6.nome, 2.0);
        mapaEstradasTeste.inserirCaminho(l2.nome, l3.nome, 8.0);
        mapaEstradasTeste.inserirCaminho(l3.nome, l9.nome, 2.0);
        mapaEstradasTeste.inserirCaminho(l3.nome, l7.nome, 1.0);
        mapaEstradasTeste.inserirCaminho(l3.nome, l8.nome, 7.0);
        mapaEstradasTeste.inserirCaminho(l9.nome, l8.nome, 3.0);
        mapaEstradasTeste.inserirCaminho(l5.nome, l8.nome, 6.0);
        mapaEstradasTeste.inserirCaminho(l7.nome, l4.nome, 1.0);
        mapaEstradasTeste.inserirCaminho(l4.nome, l5.nome, 10.0);
        mapaEstradasTeste.inserirCaminho(l7.nome, l10.nome, 5.0);
        mapaEstradasTeste.inserirCaminho(l4.nome, l5.nome, 7.0);
        mapaEstradasTeste.inserirCaminho(l10.nome, l13.nome, 12.0);
        mapaEstradasTeste.inserirCaminho(l10.nome, l3.nome, 3.0);
        mapaEstradasTeste.inserirCaminho(l11.nome, l12.nome, 9.0);
        mapaEstradasTeste.inserirCaminho(l12.nome, l1.nome, 6.0);
        mapaEstradasTeste.inserirCaminho(l13.nome, l2.nome, 6.0);
        mapaEstradasTeste.inserirCaminho(l13.nome, l6.nome, 2.0);

        p1.addLocal(l1);
        p1.addLocal(l2);
        p1.addLocal(l3);
        p1.addLocal(l4);
        p2.addLocal(l10);
        p2.addLocal(l11);
        p3.addLocal(l5);
        p3.addLocal(l6);
        p4.addLocal(l7);
        p4.addLocal(l8);
        
        mapaAliancasTeste.inserirPersonagem(p1);
        mapaAliancasTeste.inserirPersonagem(p2);
        mapaAliancasTeste.inserirPersonagem(p3);
        mapaAliancasTeste.inserirPersonagem(p4);
        
        mapaAliancasTeste.inserirAlianca(p1.nome, p2.nome, false, 0.5);
        mapaAliancasTeste.inserirAlianca(p1.nome, p3.nome, true, 0.5);
        mapaAliancasTeste.inserirAlianca(p2.nome, p3.nome, false, 0.8);
        mapaAliancasTeste.inserirAlianca(p3.nome, p4.nome, true, 0.2);
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of personagemPodeConquistarLocalComAliado method, of class ControloJogo.
     */
    @Test
    public void testPersonagemPodeConquistarLocalComAliado() {
        System.out.println("personagemPodeConquistarLocalComAliado");
        
        Personagem personagem1 = null;
        Personagem personagem2 = null;
        Local local1 = null;
        LinkedList<Local> caminhoFinal = null;
        ControloJogo instance = new ControloJogo();
        
        // teste com os valores a null
        Object[] expResult = {0, null};
        Object[] result = instance.personagemPodeConquistarLocalComAliado(personagem1, local1, caminhoFinal);
        assertArrayEquals(expResult, result);
        
        caminhoFinal = new LinkedList<>();
        personagem1 = new Personagem("teste", 100);
        personagem2 = new Personagem("teste2", 100);
        instance.getMapaAliancas().inserirPersonagem(personagem1);
        instance.getMapaAliancas().inserirPersonagem(personagem2);
        instance.getMapaAliancas().inserirAlianca(personagem1.nome, personagem2.nome, true, 0.5);
        
        // teste com um local que não existe no mapaEstradas
        local1 = new Local("teste", null, 10);
        result = instance.personagemPodeConquistarLocalComAliado(personagem1, local1, caminhoFinal);
        assertArrayEquals(expResult, result);
        assertEquals(0, caminhoFinal.size());
        
        // teste com apenas dois locais, em que a personagem é dona de um deles, mas não há caminhos que liguem um ao outro
        local1 = new Local("Local01", null, 10);
        Local local2 = new Local("Local02", personagem1, 10);
        instance.getMapaEstradas().inserirLocal(local1);
        instance.getMapaEstradas().inserirLocal(local2);
        personagem1.addLocal(local2);
        result = instance.personagemPodeConquistarLocalComAliado(personagem1, local1, caminhoFinal);
        assertArrayEquals(expResult, result);
        assertEquals(0, caminhoFinal.size());
        
        // teste com apenas dois locais, que estão ligados, mas a personagem não é dona de nenhum
        instance.getMapaEstradas().getMapaEstradas().removeVertex(local2);
        personagem1.removeLocal(local2);
        local2 = new Local("Local02", null, 10);
        instance.getMapaEstradas().inserirLocal(local2);
        instance.getMapaEstradas().inserirCaminho(local1.getNome(), local2.getNome(), 10.0);
        result = instance.personagemPodeConquistarLocalComAliado(personagem1, local2, caminhoFinal);
        assertArrayEquals(expResult, result);
        assertEquals(0, caminhoFinal.size());
        
        // teste com apenas dois locais, ligados, em que a personagem é dona dos dois
        instance.getMapaEstradas().getMapaEstradas().removeVertex(local1);
        instance.getMapaEstradas().getMapaEstradas().removeVertex(local2);
        personagem1.removeLocal(local2);
        local1 = new Local("Local01", personagem1, 10);
        local2 = new Local("Local02", personagem1, 10);
        personagem1.addLocal(local1);
        personagem1.addLocal(local2);
        instance.getMapaEstradas().inserirLocal(local1);
        instance.getMapaEstradas().inserirLocal(local2);
        instance.getMapaEstradas().inserirCaminho(local1.getNome(), local2.getNome(), 10.0);
        result = instance.personagemPodeConquistarLocalComAliado(personagem1, local1, caminhoFinal);
        assertArrayEquals(expResult, result);
        assertEquals(0, caminhoFinal.size());
        
        // teste com apenas dois locais, em que a personagem é dona de apenas um só, mas não tem aliados
        instance.getMapaAliancas().removeEdge(personagem1, personagem2);
        instance.getMapaEstradas().getMapaEstradas().removeVertex(local2);
        personagem1.removeLocal(local2);
        local2 = new Local("Local02", null, 10);
        instance.getMapaEstradas().inserirLocal(local2);
        instance.getMapaEstradas().inserirCaminho(local1.getNome(), local2.getNome(), 10.0);
        result = instance.personagemPodeConquistarLocalComAliado(personagem1, local2, caminhoFinal);
        assertArrayEquals(expResult, result);
        assertEquals(0, caminhoFinal.size());
        
        // teste com apenas dois locais, em que a personagem é dona de apenas um só e o dono do outro local é seu aliado
        instance.getMapaAliancas().inserirAlianca(personagem1.nome, personagem2.nome, true, 0.5);
        instance.getMapaEstradas().getMapaEstradas().removeVertex(local2);
        local2 = new Local("Local02", personagem2, 10);
        instance.getMapaEstradas().inserirLocal(local2);
        instance.getMapaEstradas().inserirCaminho(local1.getNome(), local2.getNome(), 10.0);
        result = instance.personagemPodeConquistarLocalComAliado(personagem1, local2, caminhoFinal);
        assertArrayEquals(expResult, result);
        assertEquals(0, caminhoFinal.size());
        
        // teste com 3 locais, ligados em linha (A-B-C, sem ligação A-C), em que a personagem é dona de A e o seu aliado é dona de B, a tentar conqiustar C
        Local local3 = new Local("Local03", null, 10);
        instance.getMapaEstradas().inserirLocal(local3);
        instance.getMapaEstradas().inserirCaminho(local2.getNome(), local3.getNome(), 10.0);
        result = instance.personagemPodeConquistarLocalComAliado(personagem1, local3, caminhoFinal);
        assertArrayEquals(expResult, result);
        assertEquals(0, caminhoFinal.size());

        // teste em que a força da aliança não é necessária para conquistar o local
        caminhoFinal.clear();
        expResult[0] = -306;
        expResult[1] = null;
        result = controloJogoTest.personagemPodeConquistarLocalComAliado(p1, l12, caminhoFinal);
        assertArrayEquals(expResult, result);
        assertEquals(2, caminhoFinal.size());
        
        // teste em que a força da aliança é não necessária para conquistar o local e o local pertence a um dos aliados
        caminhoFinal.clear();
        expResult[0] = -428;
        expResult[1] = null;
        result = controloJogoTest.personagemPodeConquistarLocalComAliado(p1, l11, caminhoFinal);
        assertArrayEquals(expResult, result);
        assertEquals(3, caminhoFinal.size());
        
        // teste em que a força da aliança é necessária para conquistar o local
        caminhoFinal.clear();
        expResult[0] = 21;
        expResult[1] = p2;
        result = controloJogoTest.personagemPodeConquistarLocalComAliado(p1, l5, caminhoFinal);
        assertArrayEquals(expResult, result);
        assertEquals(2, caminhoFinal.size());
    }

    /**
     * Test of forcaDaAlianca method, of class ControloJogo.
     */
    @Test
    public void testForcaDaAlianca() {
        System.out.println("forcaDaAlianca");
        Personagem personagem1 = null;
        Personagem personagem2 = null;
        ControloJogo instance = new ControloJogo();
        
        // teste para personagens null
        double expResult = -1;
        double result = instance.forcaDaAlianca(personagem1, personagem2);
        assertEquals(expResult, result, 0.0);
        
        // teste em que a personagem 1 não tem aliados
        personagem1 = new Personagem("teste", 100);
        personagem2 = new Personagem("teste2", 100);
        instance.getMapaAliancas().inserirPersonagem(personagem1);
        instance.getMapaAliancas().inserirPersonagem(personagem2);
        result = instance.forcaDaAlianca(personagem1, personagem2);
        assertEquals(expResult, result, 0.0);
        
        // teste em que a personagem 1 tem alianças públicas, mas não a personagem 2
        Personagem personagem3 =  new Personagem("teste3", 100);
        instance.getMapaAliancas().inserirPersonagem(personagem3);
        instance.getMapaAliancas().inserirAlianca(personagem1.nome, personagem3.nome, true, 0.5);
        result = instance.forcaDaAlianca(personagem1, personagem2);
        assertEquals(expResult, result, 0.0);
        
        // teste em que a personagem 1 tem alianças privadas, mas não a personagem 2
        instance.getMapaAliancas().removeEdge(personagem1, personagem3);
        instance.getMapaAliancas().inserirAlianca(personagem1.nome, personagem3.nome, false, 0.5);
        result = instance.forcaDaAlianca(personagem1, personagem2);
        assertEquals(expResult, result, 0.0);
        
        // teste em que a personagem 1 é aliada da personagem 3, com aliança privada
        expResult = 100;
        result = instance.forcaDaAlianca(personagem1, personagem3);
        assertEquals(expResult, result, 0.0);
        
        // teste em que a personagem 1 é aliada da personagem 3, com aliança pública
        instance.getMapaAliancas().removeEdge(personagem1, personagem3);
        instance.getMapaAliancas().inserirAlianca(personagem1.nome, personagem3.nome, true, 0.5);
        result = instance.forcaDaAlianca(personagem1, personagem3);
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of caminhoEPercorrivelPorAlianca method, of class ControloJogo.
     */
    @Test
    public void testCaminhoEPercorrivelPorAlianca() {
        System.out.println("caminhoEPercorrivelPorAlianca");
        LinkedList<Local> caminho = null;
        Personagem personagem1 = null;
        Personagem personagem2 = null;
        ControloJogo instance = new ControloJogo();
        
        // teste com os valores a null
        boolean expResult = false;
        boolean result = instance.caminhoEPercorrivelPorAlianca(caminho, personagem1, personagem2);
        assertEquals(expResult, result);
        
        personagem1 = new Personagem("teste", 100);
        personagem2 = new Personagem("teste2", 100);
        instance.getMapaAliancas().inserirPersonagem(personagem1);
        instance.getMapaAliancas().inserirPersonagem(personagem2);
        
        // teste com caminho a null
        expResult = false;
        result = instance.caminhoEPercorrivelPorAlianca(caminho, personagem1, personagem2);
        assertEquals(expResult, result);
        
        caminho = new LinkedList<>();
        
        // teste com caminho vazio
        expResult = false;
        result = instance.caminhoEPercorrivelPorAlianca(caminho, personagem1, personagem2);
        assertEquals(expResult, result);
        
        
        // teste com as personagens sem aliança entre si
        caminho.add(l1);
        caminho.add(l12);
        expResult = false;
        result = instance.caminhoEPercorrivelPorAlianca(caminho, personagem1, personagem2);
        assertEquals(expResult, result);
        
        // testes com as personagens com aliança entre si
        // teste em que a personagem 1 é dona de caminhos intermédios
        caminho.clear();
        caminho.add(l1);
        caminho.add(l2);
        expResult = false;
        result = controloJogoTest.caminhoEPercorrivelPorAlianca(caminho, p1, p2);
        assertEquals(expResult, result);
        
        // teste em que a personagem 2 é dona de caminhos intermédios
        caminho.clear();
        caminho.add(l12);
        caminho.add(l1);
        expResult = false;
        result = controloJogoTest.caminhoEPercorrivelPorAlianca(caminho, p2, p1);
        assertEquals(expResult, result);
        
        // teste em que a personagem1 não tem força suficiente para conquistar o local
        caminho.clear();
        caminho.add(l2);
        caminho.add(l13);
        expResult = false;
        result = controloJogoTest.caminhoEPercorrivelPorAlianca(caminho, p1, p2);
        assertEquals(expResult, result);
        
        // teste em que a personagem1 não tem força suficiente para conquistar o local
        caminho.clear();
        caminho.add(l6);
        caminho.add(l13);
        expResult = false;
        result = controloJogoTest.caminhoEPercorrivelPorAlianca(caminho, p2, p1);
        assertEquals(expResult, result);
        
        // teste em que, individualmente, as personagens têm força para aguentar a redução individual, mas a aliança não tem força para conquistar o local
        caminho.clear();
        caminho.add(l8);
        caminho.add(l9);
        expResult = false;
        result = controloJogoTest.caminhoEPercorrivelPorAlianca(caminho, p4, p3);
        assertEquals(expResult, result);
        
        // teste em que a aliança e as personagens têm força para conquistar o local
        caminho.clear();
        caminho.add(l4);
        caminho.add(l5);
        expResult = true;
        result = controloJogoTest.caminhoEPercorrivelPorAlianca(caminho, p1, p2);
        assertEquals(expResult, result);
    }
    
}


package Servico;

public class Senha implements Comparable<Senha>{
    private String nSenha; // Main Key
    public String nContribuinte;
    private char codigoAssunto;
    
    public Senha(String nSenha, String nContribuinte, char codigoAssunto) {
        this.nSenha = nSenha;
        this.codigoAssunto = codigoAssunto;
        this.nContribuinte = nContribuinte;
    }
    
    @Override
    public boolean equals(Object obj){
        if(obj == null && (this.getClass() != obj.getClass())) return false;
        
        Senha outraSenha = (Senha) obj;
        return this.getnContribuinte().equalsIgnoreCase(outraSenha.getnContribuinte()) 
                && this.getCodigoAssunto() == outraSenha.getCodigoAssunto();
    }

    /**
     * Returns the written info about the Senha
     * @return 
     */
    @Override
    public String toString() {
        return ("Número de Senha: " + nSenha
                + "\nCódigo do Assunto: " + codigoAssunto
                + "\nNúmero de contribuinte: " + getnContribuinte());
    }
    
    /**
     * @return the nSenha
     */
    public String getnSenha() {
        return nSenha;
    }

    /**
     * @param nSenha the nSenha to set
     */
    public void setnSenha(String nSenha) {
        this.nSenha = nSenha;
    }

    /**
     * @return the codigoAssunto
     */
    public char getCodigoAssunto() {
        return codigoAssunto;
    }

    /**
     * @param codigoAssunto the codigoAssunto to set
     */
    public void setCodigoAssunto(char codigoAssunto) {
        this.codigoAssunto = codigoAssunto;
    }

    /**
     * @return the nContribuinte
     */
    public String getnContribuinte() {
        return nContribuinte;
    }

    /**
     * @param nContribuinte the nContribuinte to set
     */
    public void setnContribuinte(String nContribuinte) {
        this.nContribuinte = nContribuinte;
    }

    @Override
    public int compareTo(Senha o) {
        int comp = this.getnSenha().compareTo(o.getnSenha());
        String assunto = String.valueOf(this.getCodigoAssunto());
        String oAssunto = String.valueOf(o.getCodigoAssunto());
        if(comp == 0) return assunto.compareTo(oAssunto);
        return comp;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servico;

import com.sun.org.apache.xalan.internal.xsltc.runtime.BasisLibrary;
import esinf.DoublyLinkedList;
import java.util.*;

/**
 *
 * @author HP
 */
public class Reparticao {
    private String cidade;
    
    private String nReparticao; //Main Key
    
    private String codigoPostal;
    
    private DoublyLinkedList<Character> listaServicos;
    
    public DoublyLinkedList<Cidadao> cidadaos;//lista de cidadaos desta reparticao
    
    public DoublyLinkedList<Senha> senhas;
    
    public Reparticao(String cidade, String nReparticao, String codigoPostal, DoublyLinkedList<Character> listaServicos){
        this.cidade = cidade;
        this.nReparticao = nReparticao;
        this.codigoPostal = codigoPostal;
        this.listaServicos = listaServicos;
        cidadaos = new DoublyLinkedList<>();
        senhas = new DoublyLinkedList<>();
    }
    
    @Override
    public boolean equals(Object obj){
        if(obj == null && (this.getClass() != obj.getClass())) return false;
        
        Reparticao outraReparticao = (Reparticao) obj;
        return this.getnReparticao().equalsIgnoreCase(outraReparticao.getnReparticao()) ||
                this.getCodigoPostal().equalsIgnoreCase(outraReparticao.getCodigoPostal());
    }
    
    @Override
    public String toString(){
        return "Cidade: " + cidade
                + "\nNúmero de Repartição: " + nReparticao
                + "\nCódigo Postal: " + codigoPostal;
    }

    /**
     * @return the cidade
     */
    public String getCidade() {
        return cidade;
    }

    /**
     * @return the nReparticao
     */
    public String getnReparticao() {
        return nReparticao;
    }

    /**
     * @return the codigoPostal
     */
    public String getCodigoPostal() {
        return codigoPostal;
    }

    /**
     * @return the listaServicos
     */
    public DoublyLinkedList<Character> getListaServicos() {
        return listaServicos;
    }

    /**
     * @param cidade the cidade to set
     */
    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    /**
     * @param nReparticao the nReparticao to set
     */
    public void setnReparticao(String nReparticao) {
        this.nReparticao = nReparticao;
    }

    /**
     * @param codigoPostal the codigoPostal to set
     */
    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    /**
     * @param listaServicos the listaServicos to set
     */
    public void setListaServicos(DoublyLinkedList<Character> listaServicos) {
        this.listaServicos = listaServicos;
    }

    /**
     * @return the cidadaos
     */
    public DoublyLinkedList<Cidadao> getCidadaos() {
        return cidadaos;
    }

    /**
     * @param cidadaos the cidadaos to set
     */
    public void setCidadaos(DoublyLinkedList<Cidadao> cidadaos) {
        this.cidadaos = cidadaos;
    }

    /**
     * @return the senhas
     */
    public DoublyLinkedList<Senha> getSenhas() {
        return senhas;
    }

    /**
     * @param senhas the senhas to set
     */
    public void setSenhas(DoublyLinkedList<Senha> senhas) {
        this.senhas = senhas;
    }
}

package Servico;

import esinf.DoublyLinkedList;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

/**
 *
 * @author HP
 */
public class CentroReparticoes {

    public DoublyLinkedList<Reparticao> reparticoes;

    public CentroReparticoes() {
        reparticoes = new DoublyLinkedList<>();
    }

    /**
     * Método para adicionar uma nova repartição, verificando se já existe
     * alguma com o mesmo código postal e adiciona todos os cidadaos com a mesmo
     * código postal da repartição a essa mesma repartição.
     *
     * @param novaReparticao A repartção a ser adicionada.
     * @return Um boolean que é true se for adicionada ou false se já existir.
     */
    public boolean adicionarReparticao(Reparticao novaReparticao) {
        if (novaReparticao == null) {
            return false;
        }

        if (reparticoes.isEmpty()) {
            reparticoes.addFirst(novaReparticao);
            return true;
        }

        String novoCodPostal = novaReparticao.getCodigoPostal();
        Iterator<Reparticao> itr = getReparticoes().iterator();
        while (itr.hasNext()) {
            if (novoCodPostal.equalsIgnoreCase(itr.next().getCodigoPostal())) {
                return false;
            }
        }
        reparticoes.addLast(novaReparticao);

        for (Reparticao r : reparticoes) {
            Iterator<Cidadao> cItr = r.getCidadaos().iterator();
            String novoCodigo = novaReparticao.getCodigoPostal();
            while (cItr.hasNext()) {
                String[] cod = cItr.next().getCodigoPostal().split("-");
                String cod1 = cod[0];
                if (novoCodigo.equalsIgnoreCase(cod1)) {
                    novaReparticao.getCidadaos().addLast(cItr.next());
                }
            }
        }

        return true;
    }
    /*
     Método para ir buscar a informacao de cada cidadão (numero de contribuinte, cidade de reparticao e numero de reparticao)
     @return uma matrix de strings com a informação.
     */

    public String[][] getInformacaoCidadao() {
        if (reparticoes.isEmpty()) {
            return null;
        }
        Iterator<Reparticao> rItr = reparticoes.iterator();
        int cont = 0;
        for (Reparticao r : reparticoes) {
            if (r.getCidadaos().isEmpty()) {
                cont++;
            }
            for (Cidadao c : r.getCidadaos()) {
                cont++;//total cidadaos
            }
        }
        String[][] informacao = new String[cont][3];
        int i = 0;
        while (rItr.hasNext()) {
            Reparticao r = rItr.next();
            String numero = r.getnReparticao();
            String cidade = r.getCidade();
            if (r.getCidadaos().isEmpty()) {
                informacao[i][0] = null;
                informacao[i][1] = numero;
                informacao[i][2] = cidade;
                i++;
            } else {
                Iterator<Cidadao> cItr = r.getCidadaos().iterator();
                while (cItr.hasNext()) {
                    Cidadao c = cItr.next();
                    String nContribuinte = c.getnContribuinte();
                    informacao[i][0] = nContribuinte;
                    informacao[i][1] = numero;
                    informacao[i][2] = cidade;
                    i++;
                }
            }
        }
        return informacao;
    }

    /**
     * Remove uma repartição recebida por parâmetro e move os cidadãos
     * associados para a repartição com o código postal mais próximo.
     *
     * @param reparticao
     * @return true se todo o processo correr de acordo com o descrito, false se
     * a lista estiver vazia, se a repartição não for encontrada ou se não for
     * encontrada uma repartição de substituição
     */
    public boolean removerReparticao(Reparticao reparticao) {

        if (reparticoes.isEmpty()) {
            return false; // se não houver repartições
        }

        Iterator<Reparticao> itrSaved = reparticoes.iterator();
        int codP1 = Integer.parseInt(reparticao.getCodigoPostal());
        List<Reparticao> reparticoesProximas = new ArrayList<>();
        int valorMin = Integer.MAX_VALUE, diferenca1;
        Reparticao substituto = null;

        boolean encontrado = false;
        while (itrSaved.hasNext() && !encontrado) { // verifica se a repartição recebida existe
            if (reparticao.getnReparticao().equals(itrSaved.next().getnReparticao())) {
                encontrado = true;
            }
        }
        if (!encontrado) {
            return false; // se não encontrar a repartição
        }
        Iterator<Reparticao> itr = reparticoes.iterator();
        int codP2;

        while (itr.hasNext()) { // adiciona as repartições com a primeira metade do código postal mais próximo a uma lista 
            Reparticao e = itr.next();
            codP2 = Integer.parseInt(e.getCodigoPostal());
            diferenca1 = Math.abs(codP1 - codP2);
            if (e.equals(reparticao)) {
            } else if (valorMin >= diferenca1) {
                substituto = e;
            }
        }

        if (substituto == null) {
            return false; // retorna false se o substituto continuar a null
        }
        Iterator<Cidadao> itrCid1 = reparticao.getCidadaos().iterator();

        while (itrCid1.hasNext()) {
            substituto.getCidadaos().addLast(itrCid1.next());
        }

        itrSaved.remove();
        return true;
    }

    /**
     * Método para inserir um cidadão numa determinada repartição. Verifica se o
     * cidadão já existe e se a sua repartição não existe Se esta não existir
     * então coloca o cidadão na repartição com menor diferença de código
     * postal.
     *
     * @param c Cidadão a ser inserido
     * @return Boolean que indica se o cidadão foi ou não inserido
     */
    public boolean inserirCidadao(Cidadao c) {
        if (c == null) {
            return false;
        }
        Iterator<Reparticao> rItr = reparticoes.iterator();
        while (rItr.hasNext()) {
            Reparticao r = rItr.next();
            Iterator<Cidadao> cItr = r.getCidadaos().iterator();
            while (cItr.hasNext()) {
                if (cItr.next().equals(c)) {
                    return false;
                }
            }
            if (r.getnReparticao().equalsIgnoreCase(c.getnReparticao())) {
                r.getCidadaos().addLast(c);
                return true;
            }
        }

        //Se não existir o cidadão nem uma repartição com o seu número
        String[] codigoPostal = c.getCodigoPostal().split("-");
        int cod = Integer.parseInt(codigoPostal[0]);
        Iterator<Reparticao> rItr1 = reparticoes.iterator();
        while (rItr1.hasNext()) {
            Reparticao r = rItr1.next();
            int codReparticao = Integer.parseInt(r.getCodigoPostal());
            int diferenca = Math.abs(cod - codReparticao);
            Reparticao r1 = rItr1.next();
            int codReparticao1 = Integer.parseInt(r1.getCodigoPostal());
            int diferenca1 = Math.abs(cod - codReparticao1);

            if (diferenca < diferenca1) {
                r.getCidadaos().addLast(c);
                return true;
            } else if (diferenca1 < diferenca) {
                r1.getCidadaos().addLast(c);
                return true;
            }
        }
        return false;
    }

    /**
     * Método que cria um mapa de senhas para cada 10 minutos que estão a ser
     * atendidas nos respetivos serviços.
     *
     * @param r Repartição em que se quer ver o mapa de senhas.
     * @return Um mapa com um set de senhas que estão a ser atendidas numa
     * determinada hora e os minutos em que estão a ser atendidas.
     */
    public Map<Integer, Set<Senha>> criarMapaDeSenhas(Reparticao r) {
        Map<Integer, Set<Senha>> mapaSenhas = new TreeMap<>();
        int minutos = 10;
        if (r == null) {
            return mapaSenhas;
        }
        while (!r.getSenhas().isEmpty()) {
            Iterator<Senha> sItr = r.getSenhas().iterator();
            Set<Senha> senhas = new TreeSet<>();
            while (sItr.hasNext()) {
                Senha s = sItr.next();
                if (senhas.isEmpty()) { //adicionar sempre o primeiro elemento ao set
                    senhas.add(s);
                    sItr.remove();
                }
                Iterator<Senha> itr = senhas.iterator();
                List<Boolean> list = new ArrayList<>();
                while (itr.hasNext()) { //Percorre o set de senhas atual
                    Senha s1 = itr.next();
                    boolean flag;
                    if (!s.getnContribuinte().equalsIgnoreCase(s1.getnContribuinte()) && s.getCodigoAssunto() != s1.getCodigoAssunto()) { //Se não existir uma senha com o numero de contribuinte e servico atual, então é adicionada ao set
                        flag = true;//se forem diferentes a flag é true
                    } else {
                        flag = false;
                    }
                    list.add(flag);//preenche-se a lista com as flags
                }
                int size = senhas.size();
                int cont = 0;
                for(Boolean b : list){
                    if(b == true){ //conta-se quantas flags true há
                        cont++;
                    }
                }
                if(cont == size){ //Apenas se toda a lista for diferente é que nos adicionamos a senha ao set.
                    senhas.add(s);
                    sItr.remove();
                }
            }
            mapaSenhas.put(minutos, senhas);
            minutos = minutos + 10;
        }
        return mapaSenhas;
    }

    /**
     * Método para inserir uma senha na repartição do seu cidadão (de acordo com
     * o seu numero de contribuinte)
     *
     * @param s Senha a ser adicionada
     * @return Boolean que indica se a senha foi adicionada com sucesso ou não
     */
    public boolean inserirSenha(Senha s) {
        if (s == null) {
            return false;
        }
        Iterator<Reparticao> rItr = reparticoes.iterator();
        while (rItr.hasNext()) {
            Reparticao r1 = rItr.next();
            Iterator<Cidadao> cItr = r1.getCidadaos().iterator();
                while (cItr.hasNext()) {
                Cidadao c = cItr.next();
                if (r1.getSenhas().isEmpty()) {
                    r1.getSenhas().addFirst(s);
                    return true;
                }
                Iterator<Senha> sItr = r1.getSenhas().iterator();
                while (sItr.hasNext()) {
                    Senha s1 = sItr.next();
                    if(s1.equals(s) && s1.getnSenha().equalsIgnoreCase(s.getnSenha())){
                        return false;
                    }
                    if (c.getnContribuinte().equalsIgnoreCase(s.getnContribuinte())) {
                        r1.getSenhas().addLast(s);
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * Determina os serviços com maior procura para um determinado dia
     *
     * @param mapaSenhas
     * @return null se a lista recebida estiver vazia ou uma lista de char
     * correspondentes ao nome dos serviços
     */
    public List<Character> servicoMaiorProcura(Map<Integer, Set<Senha>> mapaSenhas) {

        List<Character> servicos = new ArrayList<>();

        if (mapaSenhas.isEmpty()) {
            return null; // return null se o mapa recebido estiver vazio
        }
        Map<Character, Integer> ocorrencias = new TreeMap<>();
        int nOcorrencias;
        char servico;

        // cria um mapa com os serviços existentes e respetivo número de ocorrências
        for (Set<Senha> set : mapaSenhas.values()) {
            for (Senha senha : set) {
                servico = senha.getCodigoAssunto();
                if (ocorrencias.containsKey(servico)) {
                    nOcorrencias = ocorrencias.get(servico);
                    ocorrencias.replace(servico, nOcorrencias + 1);
                } else {
                    ocorrencias.put(servico, 1);
                }
            }
        }

        int valor;
        nOcorrencias = 0;

        // cria um arraylist com os chars correspondentes aos serviços com maior número de ocorrências
        for (Map.Entry<Character, Integer> entry : ocorrencias.entrySet()) {
            valor = entry.getValue();
            if (valor > nOcorrencias) {
                nOcorrencias = valor;
                servicos.clear();
                servicos.add(entry.getKey());
            } else if (valor == nOcorrencias) {
                servicos.add(entry.getKey());
            }
        }

        return servicos;
    }

    /**
     * Remove as senhas referentes a um cidadão de todas as filas
     * @param cidadao
     * @param mapaSenhas
     * @return false se a lista estiver vazia, true se o processo decorrer como esperado
     */
    public boolean abandonarFila(Cidadao cidadao, Map<Integer, Set<Senha>> mapaSenhas) {
        
        if(mapaSenhas.isEmpty()) return false;
        
        String nContribuinteCidadao = cidadao.getnContribuinte();
        Iterator<Senha> sItr;
        
        for(Set<Senha> senhas : mapaSenhas.values()) {
            Senha aRemover = null;
            for(Senha senha : senhas) {
                if(nContribuinteCidadao.equals(senha.nContribuinte)) {
                    aRemover = senha;
                }
            }
            if(aRemover != null) senhas.remove(aRemover);
        }
        
        return true;
    }
    
    /**
     * @return the reparticoes
     */
    public DoublyLinkedList<Reparticao> getReparticoes() {
        return reparticoes;
    }

    /**
     * @param reparticoes the reparticoes to set
     */
    public void setReparticoes(DoublyLinkedList<Reparticao> reparticoes) {
        this.reparticoes = reparticoes;
    }
}


package Servico;

public class Cidadao {
    private String nContribuinte; // Main Key
    private String nome;
    private String correioEletronico;
    private String codigoPostal;
    private String nReparticao; // Foreign Key
    
    public Cidadao(String nContribuinte, String nome, String correioEletronico, String codigoPostal, String nReparticao) {
        this.nContribuinte = nContribuinte;
        this.nome = nome;
        this.correioEletronico = correioEletronico;
        this.codigoPostal = codigoPostal;
        this.nReparticao = nReparticao;
    }
    
    @Override
    public boolean equals(Object obj){
        if(obj == null){
            return false;
        }
        if(obj.getClass() != this.getClass()) return false;
        
        Cidadao outroCidadao = (Cidadao) obj;
        
        return this.getnContribuinte().equalsIgnoreCase(outroCidadao.getnContribuinte());
    }

    /**
     * Returns the written info about the Cidadao
     * @return 
     */
    @Override
    public String toString() {
        return ("Número de Contribuinte: " + nContribuinte
                + "\nNome: " + nome
                + "\nCorreio Eletrónico: " + correioEletronico
                + "\nCódigo Postal: " + codigoPostal
                + "\nNúmero de Repartição: " + nReparticao);
    }
    
    /**
     * @return the nContribuinte
     */
    public String getnContribuinte() {
        return nContribuinte;
    }

    /**
     * @param nContribuinte the nContribuinte to set
     */
    public void setnContribuinte(String nContribuinte) {
        this.nContribuinte = nContribuinte;
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @return the correioEletronico
     */
    public String getCorreioEletronico() {
        return correioEletronico;
    }

    /**
     * @param correioEletronico the correioEletronico to set
     */
    public void setCorreioEletronico(String correioEletronico) {
        this.correioEletronico = correioEletronico;
    }

    /**
     * @return the codigoPostal
     */
    public String getCodigoPostal() {
        return codigoPostal;
    }

    /**
     * @param codigoPostal the codigoPostal to set
     */
    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    /**
     * @return the nReparticao
     */
    public String getnReparticao() {
        return nReparticao;
    }

    /**
     * @param nReparticao the nReparticao to set
     */
    public void setnReparticao(String nReparticao) {
        this.nReparticao = nReparticao;
    }

}

package Servico;

import esinf.DoublyLinkedList;
import java.io.IOException;
import java.util.Formatter;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeMap;

/**
 *
 * @author HP
 */
public class Main {

    public static void main(String[] args) throws IOException {
        leituraFicheiros lf = new leituraFicheiros();
        CentroReparticoes cr = lf.getCr();
        Scanner sc = new Scanner(System.in);
        lf.lerReparticoes();
        lf.lerCidadaos();
        lf.lerSenhas();
        Map<Integer, Set<Senha>> mapaSenhas = null;
        Reparticao reparticaoEscolhida = null;

        //Menu
        char op;
        System.out.println("\nMenu:\n"
                + "1. Adicionar Repartição\n"
                + "2. Remover Repartição\n"
                + "3. Informação de Cidadãos\n"
                + "4. Inserir novo cidadão\n"
                + "5. Criar mapa de senhas para uma repartição\n"
                + "6. Ver o serviço com maior procura num dia\n"
                + "7. Abandonar as filas de um cidadão\n"
                + "8. Terminar.");
        do {
            op = sc.nextLine().charAt(0);
            switch (op) {
                case '1':
                    System.out.println("Introduza os dados da repartição a ser adicionada.");
                    System.out.println("Cidade: ");
                    String cidade = sc.nextLine();
                    System.out.println("número de Repartição: ");
                    String nReparticao = sc.nextLine();
                    System.out.println("código Postal: ");
                    String codigoPostal = sc.nextLine();
                    System.out.println("Lista de serviços: ");
                    String lista = sc.nextLine();
                    DoublyLinkedList<Character> l = new DoublyLinkedList<>();
                    for (int i = 0; i < lista.length(); i++) {
                        l.addLast(lista.charAt(i));
                    }
                    Reparticao r = new Reparticao(cidade, nReparticao, codigoPostal, l);
                    boolean flag = cr.adicionarReparticao(r);
                    if (flag == false) {
                        System.out.println("Repartição inválida ou já existente.");
                    } else {
                        System.out.println("Repartição adicionada.");
                    }
                    break;

                case '2':
                    System.out.println("Introduza o número da repartição a ser removida:");
                    System.out.println("número de Repartição: ");
                    String nReparticao1 = sc.nextLine();
                    Reparticao r1 = null;
                    for (Reparticao r4 : cr.getReparticoes()) {
                        if (r4.getnReparticao().equalsIgnoreCase(nReparticao1)) {
                            r1 = r4;
                        }
                    }
                    boolean flag1 = cr.removerReparticao(r1);
                    if (flag1 == false) {
                        System.out.println("Repartição inválida ou inexistente.");
                    } else {
                        System.out.println("Repartição removida.");
                    }
                    break;
                case '3':
                    String[][] informacao = cr.getInformacaoCidadao();
                    for (int i = 0; i < informacao.length; i++) {
                        for (int j = 0; j < informacao[i].length; j++) {
                            System.out.println(informacao[i][j] + " ");
                            Formatter output = new Formatter();
                            output.format("%s", informacao[i][j]);
                        }
                        System.out.println();
                    }
                    break;
                case '4':
                    System.out.println("Introduza os dados para inserir um novo cidadão:");
                    System.out.println("Introduza o número de contribuinte: ");
                    String nContribuinte = sc.nextLine();
                    System.out.println("Introduza o nome: ");
                    String nome = sc.nextLine();
                    System.out.println("Introduza o e-mail: ");
                    String email = sc.nextLine();
                    System.out.println("Introduza o codigo postal: ");
                    String cod = sc.nextLine();
                    System.out.println("Introduza o número da reparticao a que este pertence: ");
                    String nRep = sc.nextLine();
                    Cidadao c = new Cidadao(nContribuinte, nome, email, cod, nRep);
                    boolean flag2 = cr.inserirCidadao(c);
                    if (flag2 == false) {
                        System.out.println("Cidadão inválido ou já existente.");
                    } else {
                        System.out.println("Cidadão adicionado.");
                    }
                    break;
                case '5':
                    System.out.println("Introduza o número de repartição: ");
                    String nRep1 = sc.nextLine();
                    for (Reparticao rep : cr.getReparticoes()) {
                        if (rep.getnReparticao().equalsIgnoreCase(nRep1)) {
                            reparticaoEscolhida = rep;
                        }
                    }
                    mapaSenhas = cr.criarMapaDeSenhas(reparticaoEscolhida);
                    if (mapaSenhas.isEmpty()) {
                        System.out.println("Repartição inexistente ou vazia.");
                    } else {
                        System.out.println(mapaSenhas.toString());
                    }
                    break;
                case '6':
                    System.out.println("Serviço com maior procura no dia da repartição escolhida na criação do mapa: ");
                    if (mapaSenhas.isEmpty()) {
                        System.out.println("por favor introduza uma repartição no ponto 5.");
                    } else {
                        List<Character> servicos = cr.servicoMaiorProcura(mapaSenhas);
                        System.out.println(servicos.toString());
                    }
                    break;
                case '7':
                    System.out.println("Inserir contribuinte do cidadão que pretende abandonar as filas: (A repartição será a do mapa criado na opção 5)");
                    String contribuinte = sc.nextLine();
                    boolean flag5 = false;
                    if (mapaSenhas.isEmpty() || reparticaoEscolhida.getCidadaos().isEmpty()) {
                        System.out.println("Não há cidadãos nesta repartição ou não foi introduzida uma repartição no ponto 5");
                        break;
                    } else {
                        for (Cidadao cidadao : reparticaoEscolhida.getCidadaos()) {
                            if (cidadao.getnContribuinte().equalsIgnoreCase(contribuinte)) {
                                flag5 = cr.abandonarFila(cidadao, mapaSenhas);
                            }
                            if (flag5 == true) {
                                System.out.println("Fila do cidadão " + contribuinte + " abandonada.");
                            } else {
                                System.out.println("Mapa vazio ou cidadão inexistente.");
                            }
                        }
                        break;
                    }
                case '8':
                    System.out.println("Programa terminado.");
                    break;
                default:
                    System.out.println("Opção inválida.");
                    break;
            }
        } while (op != '8');
    }
}

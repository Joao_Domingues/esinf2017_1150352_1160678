package Servico;

import esinf.DoublyLinkedList;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

/**
 *
 * @author HP
 */
public class leituraFicheiros {

    public CentroReparticoes cr;

    public leituraFicheiros() {
        this.cr = new CentroReparticoes();
    }

    /**
     * Método para ler o ficheiro das repartições.
     *
     * @return boolean que indica se o ficheiro foi lido com sucesso.
     * @throws IOException Exceção lançada no caso de não encontrar o ficheiro
     * desejado.
     */
    public boolean lerReparticoes() throws IOException {
        boolean flag = true;
        try {
            List<String> reparticoes = Files.lines(Paths.get("fx_reparticoes.txt")).collect(Collectors.toList());

            for (String s : reparticoes) {
                String[] rep = s.split(",");
                DoublyLinkedList<Character> listaServicos = new DoublyLinkedList<>();
                for (int i = 3; i < rep.length; i++) {
                    listaServicos.addLast(rep[i].charAt(0));
                }
                Reparticao r = new Reparticao(rep[0], rep[1], rep[2], listaServicos);
                getCr().adicionarReparticao(r);
            }
        } catch (IOException ex) {
            flag = false;
            System.out.println(ex.getMessage());
        }
        return flag;
    }

    /**
     * Método para ler o ficheiro dos cidadão.
     *
     * @return boolean que indica se o ficheiro foi lido com sucesso.
     * @throws IOException Exceção lançada quando o ficheiro desejado não é
     * encontrado.
     */
    public boolean lerCidadaos() throws IOException {
        boolean flag = true;
        try {
            List<String> cidadaos = Files.lines(Paths.get("fx_cidadaos.txt")).collect(Collectors.toList());

            for (String s : cidadaos) {
                String[] cidadao = s.split(",");
                Cidadao c = new Cidadao(cidadao[1], cidadao[0], cidadao[2], cidadao[3], cidadao[4]);
                getCr().inserirCidadao(c);
            }

        } catch (IOException ex) {
            flag = false;
            System.out.println(ex.getMessage());
        }
        return flag;
    }
    
    /**
     * Método para ler o ficheiro das cenas e inseri-las nas respetivas repartições.
     * @return boolean que indica se o ficheiro foi lido com sucesso.
     * @throws IOException Exceção que indica se o ficheiro foi encontrado.
     */
    public boolean lerSenhas() throws IOException {
        boolean flag = true;
        try {
            List<String> senhas = Files.lines(Paths.get("fx_senhas.txt")).collect(Collectors.toList());
            
            for(String s : senhas){
                String[] senha = s.split(",");
                Senha se = new Senha(senha[2], senha[0], senha[1].charAt(0));
                getCr().inserirSenha(se);
            }
        } catch(IOException ex){
            flag = false;
            System.out.println(ex.getMessage());
        }
        return flag;
    }

    /**
     * @return the cr
     */
    public CentroReparticoes getCr() {
        return cr;
    }
}

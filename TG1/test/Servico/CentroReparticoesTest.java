/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servico;

import esinf.DoublyLinkedList;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author HP
 */
public class CentroReparticoesTest {

    public CentroReparticoesTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of adicionarReparticao method, of class CentroReparticoes.
     */
    @Test
    public void testAdicionarReparticao() {
        System.out.println("adicionarReparticao");
        Reparticao novaReparticao = null;
        CentroReparticoes instance = new CentroReparticoes();
        boolean expResult = false;
        boolean result = instance.adicionarReparticao(novaReparticao);
        assertEquals(expResult, result);

        DoublyLinkedList<Character> lista1 = new DoublyLinkedList<>();
        lista1.addLast('A');
        Reparticao r1 = new Reparticao("cidade1", "n1", "4455", lista1);
        boolean result1 = instance.adicionarReparticao(r1);
        assertEquals(true, result1);

        Reparticao r2 = new Reparticao("cidade1", "n1", "4455", lista1);
        boolean result2 = instance.adicionarReparticao(r2);
        assertEquals(false, result2);

        Reparticao r3 = new Reparticao("cidade2", "n2", "4450", lista1);
        boolean result3 = instance.adicionarReparticao(r3);
        assertEquals(true, result3);
    }

    /**
     * Test of getInformacaoCidadao method, of class CentroReparticoes.
     */
    @Test
    public void testGetInformacaoCidadao() {
        System.out.println("getInformacaoCidadao");
        CentroReparticoes instance = new CentroReparticoes();
        String[][] expResult = null;
        String[][] result = instance.getInformacaoCidadao();
        assertArrayEquals(expResult, result);

        DoublyLinkedList<Reparticao> reparticoes = instance.getReparticoes();
        DoublyLinkedList<Character> lista2 = new DoublyLinkedList<>();
        lista2.addLast('B');
        lista2.addLast('V');
        reparticoes.addFirst(new Reparticao("cidade1", "n1", "4455", lista2));
        Iterator<Reparticao> rItr = reparticoes.iterator();
        DoublyLinkedList<Cidadao> cidadaos = rItr.next().getCidadaos();
        cidadaos.addFirst(new Cidadao("123456789", "Vasco", "vascoCampos@gmail.com", "4455", "n1"));

        reparticoes.addLast(new Reparticao("cidade2", "n2", "4450", lista2));

        String[][] informacao = instance.getInformacaoCidadao();

        assertEquals("123456789", informacao[0][0]);
        assertEquals("n1", informacao[0][1]);
        assertEquals("cidade1", informacao[0][2]);

        assertEquals(null, informacao[1][0]);
        assertEquals("n2", informacao[1][1]);
        assertEquals("cidade2", informacao[1][2]);
    }

    /**
     * Test of removerReparticao method, of class CentroReparticoes
     */
    @Test
    public void testRemoverReparticao() {
        System.out.println("removerReparticao");
        CentroReparticoes instance = new CentroReparticoes();

        DoublyLinkedList<Reparticao> reparticoes = instance.getReparticoes();

        Reparticao reparticao1 = new Reparticao("cidade1", "n1", "4455", new DoublyLinkedList<>());
        Reparticao reparticao2 = new Reparticao("cidade2", "n2", "4434", new DoublyLinkedList<>());
        Reparticao reparticao3 = new Reparticao("cidade3", "n3", "4434", new DoublyLinkedList<>());
        Reparticao reparticao4 = new Reparticao("cidade4", "n4", "4436", new DoublyLinkedList<>());
        Reparticao reparticao5 = new Reparticao("cidade5", "n5", "4436", new DoublyLinkedList<>());

        reparticoes.addLast(reparticao1);
        reparticoes.addLast(reparticao2);
        reparticoes.addLast(reparticao3);
        reparticoes.addLast(reparticao4);
        reparticoes.addLast(reparticao5);

        Iterator<Reparticao> rItr = reparticoes.iterator();
        DoublyLinkedList<Cidadao> cidadaos = rItr.next().getCidadaos();

        Cidadao cidadao = new Cidadao("123456789", "Vasco", "vascoCampos@gmail.com", "4455-001", "n1");
        reparticao1.cidadaos.addFirst(cidadao);
        cidadaos.addFirst(cidadao);

        boolean result = instance.removerReparticao(reparticao1);
        assertEquals(true, result);

        String actual = reparticoes.last().cidadaos.last().getnContribuinte();
        String expResult = cidadao.getnContribuinte();
        assertEquals(expResult, actual);

    }

    /**
     * Test of inserirCidadao method, of class CentroReparticoes.
     */
    @Test
    public void testInserirCidadao() {
        System.out.println("inserirCidadao");
        Cidadao c = null;
        CentroReparticoes instance = new CentroReparticoes();
        boolean expResult = false;
        boolean result = instance.inserirCidadao(c);
        assertEquals(expResult, result);

        DoublyLinkedList<Character> lista = new DoublyLinkedList<>();
        lista.addLast('A');
        lista.addLast('H');

        Reparticao reparticao1 = new Reparticao("cidade1", "n1", "4455", lista);
        Reparticao reparticao2 = new Reparticao("cidade2", "n2", "4434", lista);
        Reparticao reparticao3 = new Reparticao("cidade3", "n3", "4460", lista);

        instance.adicionarReparticao(reparticao1);
        instance.adicionarReparticao(reparticao2);
        instance.adicionarReparticao(reparticao3);

        Cidadao c1 = new Cidadao("123456789", "Vasco", "vascocampos@gmail.com", "4455-004", "n1");
        Cidadao c2 = new Cidadao("123456789", "Vasco", "vascocampos@gmail.com", "4455-004", "n1");
        Cidadao c3 = new Cidadao("123456789", "Maria", "MariaSilva@gmail.com", "4450-123", "n1");
        Cidadao c4 = new Cidadao("987654321", "Joao", "joao@gmail.com", "4451-876", "n4");
        Cidadao c5 = new Cidadao("579257294", "Manuel", "Manuel@gmail.com", "4438-347", "n5");

        //Teste de simples inserção
        boolean result1 = instance.inserirCidadao(c1);
        assertEquals(true, result1);

        //Teste para ver se não adiciona duas instancias iguas
        boolean result2 = instance.inserirCidadao(c2);
        assertEquals(false, result2);

        //Teste para ver se não adiciona duas instancias só com o mesmo número de contribuinte, testando também o equals.
        boolean result3 = instance.inserirCidadao(c3);
        assertEquals(false, result3);

        //Teste para ver se um cidadão cuja repartição não exista é adicionado há mais próxima
        boolean result4 = instance.inserirCidadao(c4);
        assertEquals(true, result4);

        //Igual ao anterior mas com uma diferenca diferente
        boolean result5 = instance.inserirCidadao(c5);
        assertEquals(true, result5);
    }

    /**
     * Test of criarMapaDeSenhas method, of class CentroReparticoes.
     */
    @Test
    public void testCriarMapaDeSenhas() {
        System.out.println("criarMapaDeSenhas");
        CentroReparticoes instance = new CentroReparticoes();
        Map<Integer, Set<Senha>> expResult = new TreeMap<>();

        DoublyLinkedList<Character> lista = new DoublyLinkedList<>();
        lista.addFirst('A');
        lista.addLast('B');
        lista.addLast('C');

        Reparticao r1 = new Reparticao("cidade1", "1", "4455", lista);
        Map<Integer, Set<Senha>> result = instance.criarMapaDeSenhas(r1);
        assertEquals(expResult, result);
        
        instance.adicionarReparticao(r1);

        Cidadao c1 = new Cidadao("123456789", "Vasco", "vasco@gmail.com", "4455-004", "1");
        Cidadao c2 = new Cidadao("987654321", "Maria", "maria@gmail.com", "4450-123", "1");
        Cidadao c3 = new Cidadao("659304759", "Fernando", "Fernando@gmail.com", "4460-432", "1");
        Cidadao c4 = new Cidadao("085739243", "Beatriz", "Beatriz@gmail.com", "4443-004", "1");
        Cidadao c5 = new Cidadao("532058103", "Tomas", "Tomas@gmail.com", "4455-004", "1");
        Cidadao c6 = new Cidadao("941239865", "Ana", "Ana@gmail.com", "4455-004", "1");
        Cidadao c7 = new Cidadao("189572365", "Leonardo", "Leonardo@gmail.com", "4455-004", "1");
        
        instance.inserirCidadao(c1);
        instance.inserirCidadao(c2);
        instance.inserirCidadao(c3);
        instance.inserirCidadao(c4);
        instance.inserirCidadao(c5);
        instance.inserirCidadao(c6);
        instance.inserirCidadao(c7);
        
        Senha s11 = new Senha("1", "123456789", 'A');//set1
        Senha s12 = new Senha("1", "123456789", 'B');//set2
        Senha s13 = new Senha("1", "123456789", 'C');//set3
        Senha s21 = new Senha("2", "987654321", 'B');//set1
        Senha s22 = new Senha("2", "987654321", 'C');//set2
        Senha s31 = new Senha("2", "659304759", 'A');//set2
        Senha s41 = new Senha("3", "085739243", 'B');//set3
        Senha s42 = new Senha("3", "085739243", 'C');//set1
        Senha s61 = new Senha("3", "941239865", 'A');//set3
        Senha s62 = new Senha("4", "941239865", 'C');//set4
        Senha s71 = new Senha("4", "189572365", 'A');//set4
        Senha s72 = new Senha("4", "189572365", 'B');//set4
        Senha s73 = new Senha("5", "189572365", 'C');//set5
        
        instance.inserirSenha(s11);
        instance.inserirSenha(s12);
        instance.inserirSenha(s13);
        instance.inserirSenha(s21);
        instance.inserirSenha(s22);
        instance.inserirSenha(s31);
        instance.inserirSenha(s41);
        instance.inserirSenha(s42);
        instance.inserirSenha(s61);
        instance.inserirSenha(s62);
        instance.inserirSenha(s71);
        instance.inserirSenha(s72);
        instance.inserirSenha(s73);
        
        Map<Integer, Set<Senha>> result1 = instance.criarMapaDeSenhas(r1);
        Map<Integer, Set<Senha>> resultadoEsperado = new TreeMap<>();
        Set<Senha> set1 = new TreeSet<>();
        Set<Senha> set2 = new TreeSet<>();
        Set<Senha> set3 = new TreeSet<>();
        Set<Senha> set4 = new TreeSet<>();
        Set<Senha> set5 = new TreeSet<>();
        Set<Senha> set6 = new TreeSet<>();
        set1.add(s11);
        set1.add(s21);
        set1.add(s42);
        set2.add(s12);
        set2.add(s22);
        set2.add(s31);
        set3.add(s13);
        set3.add(s41);
        set3.add(s61);
        set4.add(s62);
        set4.add(s71);
        set5.add(s72);
        set6.add(s73);
        
        resultadoEsperado.put(10, set1);
        resultadoEsperado.put(20, set2);
        resultadoEsperado.put(30, set3);
        resultadoEsperado.put(40, set4);
        resultadoEsperado.put(50, set5);
        resultadoEsperado.put(60, set6);
        
        assertEquals(resultadoEsperado, result1);
    }

    /**
     * Test of servicoMaiorProcura method, of class CentroReparticoes.
     */
    @Test
    public void testServicoMaiorProcura() {

        System.out.println("servicoMaiorProcura");

        CentroReparticoes instance = new CentroReparticoes();
        Map<Integer, Set<Senha>> mapaSenhas = new TreeMap<>();
        List<Character> actual = new ArrayList<>();
        actual = instance.servicoMaiorProcura(mapaSenhas);

        assertEquals(null, actual);

        Senha senha1 = new Senha("n1", "111111111", 'A');
        Senha senha2 = new Senha("n2", "111111112", 'B');
        Senha senha3 = new Senha("n3", "111111113", 'C');
        Senha senha4 = new Senha("n4", "111111114", 'D');

        Set<Senha> set1 = new TreeSet<>();
        Set<Senha> set2 = new TreeSet<>();
        Set<Senha> set3 = new TreeSet<>();

        set1.add(senha1);
        set1.add(senha2);
        set1.add(senha4);
        set2.add(senha1);
        set3.add(senha2);
        set3.add(senha3);

        mapaSenhas.put(10, set1);
        mapaSenhas.put(20, set2);
        mapaSenhas.put(30, set3);

        List<Character> expResult = new ArrayList<>();
        expResult.add('A');
        expResult.add('B');

        actual = instance.servicoMaiorProcura(mapaSenhas);

        assertEquals(expResult, actual);
    }

    /**
     * Test of inserirSenha method, of class CentroReparticoes.
     */
    @Test
    public void testInserirSenha() {
        System.out.println("inserirSenha");
        Senha s = null;
        CentroReparticoes instance = new CentroReparticoes();
        boolean expResult = false;
        boolean result = instance.inserirSenha(s);
        assertEquals(expResult, result);

        DoublyLinkedList<Character> lista = new DoublyLinkedList<>();
        lista.addFirst('A');
        lista.addLast('B');
        lista.addLast('C');
        lista.addLast('D');
        Reparticao r = new Reparticao("cidade1", "1", "4455", lista);

        instance.adicionarReparticao(r);

        Cidadao c1 = new Cidadao("123456789", "Vasco", "vasco@gmail.com", "4455", "1");
        Cidadao c2 = new Cidadao("987654321", "Maria", "maria@gmail.com", "4450", "1");
        Cidadao c3 = new Cidadao("454657874", "João", "joao@gmail.com", "4455", "1");

        instance.inserirCidadao(c1);
        instance.inserirCidadao(c2);
        instance.inserirCidadao(c3);

        Senha s11 = new Senha("1", "123456789", 'A');
        assertEquals(true, instance.inserirSenha(s11));
    }
    
    @Test
    public void testAbandonarFila() {
        
        System.out.println("abandonarFila");
        
        CentroReparticoes instance = new CentroReparticoes();
        Map<Integer, Set<Senha>> mapaSenhas = new TreeMap<>();
        Cidadao cidadao = new Cidadao("123456789", "Vasco", "vasco@gmail.com", "4455-004", "1");
        boolean actual, expResult;
        
        actual = instance.abandonarFila(cidadao, mapaSenhas);
        expResult = false;
        
        assertEquals(expResult, actual);
        
        Senha senha1 = new Senha("n1", "123456789", 'A');
        Senha senha2 = new Senha("n2", "111111112", 'B');
        Senha senha3 = new Senha("n3", "111111113", 'C');
        Senha senha4 = new Senha("n4", "111111114", 'D');
        
        Set<Senha> set1 = new TreeSet<>();
        Set<Senha> set2 = new TreeSet<>();
        Set<Senha> set3 = new TreeSet<>();
        
        set1.add(senha2);
        set1.add(senha4);
        set1.add(senha1);
        set2.add(senha1);
        set3.add(senha2);
        set3.add(senha3);
        
        mapaSenhas.put(10, set1);
        mapaSenhas.put(20, set2);
        mapaSenhas.put(30, set3);
        
        actual = instance.abandonarFila(cidadao, mapaSenhas);
        expResult = true;
        assertEquals(expResult, actual);
        
        actual = set1.contains(senha1);
        expResult = false;
        assertEquals(expResult, actual);
        
        actual = set2.contains(senha1);
        expResult = false;
        assertEquals(expResult, actual);
        
    }
}

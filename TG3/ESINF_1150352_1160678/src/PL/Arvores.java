/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PL;

import PL.BST.Node;
import esinf_1150352_1160678.Poligono;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author HP
 */
public class Arvores {

    private AVL<Poligono> unidades;
    private AVL<Poligono> dezenas;
    private AVL<Poligono> centenas;
    private AVL<Poligono> completo;

    /**
     * Determina o nome do polígono com um determinado número de lados
     * @param nLados
     * @return 
     */
    public String nomePoligonoDeXLados(int nLados) {
        String nome = "";
        if(nLados <= 0) return nome;
        
        int resto = nLados % 100;
        if(resto > 10 && resto < 20) {
            nome = nomePoligonoDeXLadosAuxiliar(resto, dezenas);
            nLados = nLados / 100;
        } else {
            resto = nLados % 10;
            nome = nomePoligonoDeXLadosAuxiliar(resto, unidades) + nome;
            nLados = nLados / 10;
            resto = nLados % 10;
            nome = nomePoligonoDeXLadosAuxiliar(resto*10, dezenas) + nome;
            nLados = nLados / 10;
        }

        resto = nLados % 10;
        nome = nomePoligonoDeXLadosAuxiliar(resto*100, centenas) + nome;
        
        if("".equals(nome)) return nome;
        else return nome + "gon";
    }
    
    /**
     * Método auxiliar do nomePoligonoDeXLados
     * @param valor
     * @param arvore
     * @return 
     */
    private String nomePoligonoDeXLadosAuxiliar(int valor, AVL<Poligono> arvore) {
        if(arvore.isEmpty()) return "";
        
        ArrayList<Poligono> lista = (ArrayList<Poligono>) arvore.inOrder();
        if(lista.isEmpty()) return "";
        
        for(Poligono poli : lista) {
            if(poli.lados == valor) return poli.designacao;
        }
        
        return "";
    }
    
    /**
     * Determina o número de lados de um polígono de um dado nome
     * @param nome
     * @return 
     */
    public int numeroDeLadosDePoligono(String nome) {
        if(nome == null || nome.isEmpty()) return 0;
        if(completo == null || completo.isEmpty()) return 0;
        
        ArrayList<Poligono> lista = (ArrayList<Poligono>) completo.inOrder();
        if(lista.isEmpty()) return 0;
        
        for(Poligono poli : lista) {
            if(poli.designacao.equals(nome)) return poli.lados;
        }
        
        return 0;
    }
    
    /**
     * Determina o antecessor comum mais baixo na árvore de dois elementos dados
     * @param poli1
     * @param poli2
     * @return 
     */
    public Poligono lowestCommonAncestor(Poligono poli1, Poligono poli2) {
        if(poli1 == null || poli2 == null) return null;
        if(completo == null || completo.isEmpty()) return null;
        return searchLowestCommonAncestor(poli1, poli2, completo.root);
    }
    
    /**
     * Método auxiliar de lowestCommonAncestor
     * @param poli1
     * @param poli2
     * @param root
     * @return 
     */
    private Poligono searchLowestCommonAncestor(Poligono poli1, Poligono poli2, Node<Poligono> root) {
        if(poli1.equals(root)) return poli1;
        if(poli2.equals(root)) return poli2;
        int nLados = root.getElement().lados;
        if(poli1.lados < nLados && poli2.lados > nLados) return root.getElement();
        if(poli1.lados > nLados && poli2.lados < nLados) return root.getElement();
        if(poli1.lados > nLados && poli2.lados > nLados) return searchLowestCommonAncestor(poli1, poli2, root.getRight());
        if(poli1.lados < nLados && poli2.lados < nLados) return searchLowestCommonAncestor(poli1, poli2, root.getLeft());
        return root.getElement();
    }
    
    /**
     * Número de posições que a arvore das unidades terá
     */
    private final int LIMITE_UNIDADES = 9;
    /**
     * Número de posições que a arvore das dezenas terá
     */
    private final int LIMITE_DEZENAS = 27;
    /**
     * Número de posições que a arvore das centenas terá
     */
    private final int LIMITE_CENTENAS = 9;

    public Arvores() {
        unidades = new AVL<>();
        dezenas = new AVL<>();
        centenas = new AVL<>();
        //listaUnidades = new Poligono[LIMITE_UNIDADES];
        //listaDezenas = new Poligono[LIMITE_DEZENAS];
        //listaCentenas = new Poligono[LIMITE_CENTENAS];
    }
    
    /**
     * Método para inserir um array de poligonos na arvore das unidades
     * @param listaUnidades array dos poligonos
     * @return boolean que indica se foi possivel inserir a lista na arvore
     */
    public boolean inserirUnidades(Poligono[] listaUnidades) {
        if (listaUnidades.length != LIMITE_UNIDADES) {
            return false;
        }

        for (int i = 0; i < LIMITE_UNIDADES; i++) {
            getUnidades().insert(listaUnidades[i]);
        }
        return true;
    }
    
    /**
     * Método para inserir um array de poligonos na arvore das dezenas
     * @param listaDezenas array dos poligonos
     * @return boolean que indica se foi possivel inserir a lista na arvore
     */
    public boolean inserirDezenas(Poligono[] listaDezenas) {
        if (listaDezenas.length != LIMITE_DEZENAS) {
            return false;
        }

        for (int i = 0; i < LIMITE_DEZENAS; i++) {
            getDezenas().insert(listaDezenas[i]);
        }
        return true;
    }
    
    /**
     * Método para inserir um array de poligonos na arvore das centenas
     * @param listaCentenas array dos poligonos
     * @return boolean que indica se foi possivel inserir a lista na arvore
     */
    public boolean inserirCentenas(Poligono[] listaCentenas) {
        if (listaCentenas.length != LIMITE_CENTENAS) {
            return false;
        }

        for (int i = 0; i < LIMITE_CENTENAS; i++) {
            getCentenas().insert(listaCentenas[i]);
        }
        return true;
    }
    
    /**
     * Método que cria uma árvore balanceada que contém todos os nomes dos poligonos que têm lados entre 1 e 999.
     * @return A arvore com todos os nomes dos poligonos.
     */
    public AVL<Poligono> todosPoligonos() {
        AVL<Poligono> todosPoligonos = new AVL<>();

        Poligono[] designacoes = new Poligono[999];

        for (int i = 1; i < 1000; i++) {
            designacoes[i-1] = new Poligono(nomePoligonoDeXLados(i), i);
        }
        
        for (int i = 0; i < 999; i++) {
            todosPoligonos.insert(designacoes[i]); //inserir todas os poligonos na nova arvore.
        }

        return todosPoligonos;
    }
    
    /**
     * Método que retorna uma lista dos nomes dos poligonos que se encontram dentro do intervalos de lados dado.
     * @param lados1 número de lados a partir de onde o intervalo começa
     * @param lados2 número de lados a partir de onde o intervalo acaba
     * @return Uma lista com as designações de todos os poligonos que se encontram dentro do intervalo de lados dado, por ordem decrescente de lados
     */
    public List<String> listaIntervalo(int lados1, int lados2){
        List<String> listaNomes = null;
        
        if(lados1 <= 0 || lados1 >= 1000 || lados2 <= 0  || lados2 >= 1000) return listaNomes;
        
        listaNomes = new ArrayList<>();
        AVL<Poligono> arvoreCompleta = todosPoligonos();
        ArrayList<Poligono> inOrder = (ArrayList<Poligono>) arvoreCompleta.inOrder();
        for(Poligono p : inOrder){
            listaNomes.add(p.getDesignacao()); //lista ordenada com todos os nomes dos poligonos de 1 a 999
        }
        
        List<String> subList = (List<String>) listaNomes.subList(lados1-1, lados2); //lista em ordem crescente no intervalo dos dois lados
        
        Collections.reverse(subList); //Lista ordenada em ordem decrescente.
        
        return subList;
    }

    /**
     * @return the unidades
     */
    public AVL<Poligono> getUnidades() {
        return unidades;
    }

    /**
     * @return the dezenas
     */
    public AVL<Poligono> getDezenas() {
        return dezenas;
    }

    /**
     * @return the centenas
     */
    public AVL<Poligono> getCentenas() {
        return centenas;
    }
    
    public void setCompleto(AVL<Poligono> completo) {
        this.completo = completo;
    }
}

package PL;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Deque;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Stack;

/*
 * @author DEI-ESINF
 * @param <E>
 */
public class TREE<E extends Comparable<E>> extends BST<E> {

    /*
     * @param element A valid element within the tree
     * @return true if the element exists in tree false otherwise
     */
    public boolean contains(E element) {
        return (find(element, root) != null);
    }

//    public boolean containsMec(Node<E> n, E el){
//        if(n == null) return false;
//        
//        E eln = n.getElement();
//        int cmp = el.compareTo(eln);
//        
//        if(cmp == 0) return true;
//        else if(cmp < 0) return containsMec(n.getLeft(), el);
//        else return containsMec(n.getRight(), el);
//    }
    /**
     * ver se é uma folha
     *
     * @param element elemento a ser verificado
     * @return boolean que indica se é folha ou não
     */
    public boolean isLeaf(E element) {
        Node<E> n = find(element, root);
        if (n == null) {
            return false;
        }

        return (n.getLeft() == null) && (n.getRight() == null);
    }

    /*
     * build a list with all elements of the tree. The elements in the 
     * left subtree in ascending order and the elements in the right subtree 
     * in descending order. 
     *
     * @return    returns a list with the elements of the left subtree 
     * in ascending order and the elements in the right subtree is descending order.
     */
    public Iterable<E> ascdes() {
        List<E> result = new ArrayList<>();
        if (root == null) {
            return result;
        }

        ascSubtree(root.getLeft(), result);
        
        result.add(root.getElement());

        desSubtree(root.getRight(), result);

        return result;
    }

    public void inOrder(Node<E> node, List<E> snapshot) {
        if (node == null) {
            return;
        }

        inOrder(node.getLeft(), snapshot);
        snapshot.add(node.getElement());
        inOrder(node.getRight(), snapshot);
    }

    private void ascSubtree(Node<E> node, List<E> snapshot) {
        if(node == null) return;
        
        ascSubtree(node.getLeft(), snapshot);
        snapshot.add(node.getElement());
        ascSubtree(node.getRight(), snapshot);
    }

    private void desSubtree(Node<E> node, List<E> snapshot) {
        if(node == null) return;
        
        desSubtree(node.getRight(), snapshot);
        snapshot.add(node.getElement());
        desSubtree(node.getLeft(), snapshot);
    }

    /**
     * Returns the tree without leaves.
     *
     * @return tree without leaves
     */
    public BST<E> autumnTree() {
        BST<E> newTree = new TREE();
        newTree.root = copyRec(root);
        return newTree;
    }

    private boolean isLeaf(Node<E> node) {
        return (node == null) ? false : (node.getLeft() == null && node.getRight() == null);
    }

    private Node<E> copyRec(Node<E> node) {
        if ((node == null) || isLeaf(node)) {
            return null;
        }
        return new Node(node.getElement(), copyRec(node.getLeft()), copyRec(node.getRight()));
    }

    /**
     * @return the the number of nodes by level.
     */
    public int[] numNodesByLevel() {
        int[] a = new int[height() + 1];
        numNodesByLevel(root, a, 0);
        return a;
    }

    private void numNodesByLevel(Node<E> node, int[] result, int level) {
        if (node == null) {
            return;
        }

        result[level]++;

        numNodesByLevel(node.getLeft(), result, level + 1);
        numNodesByLevel(node.getRight(), result, level + 1);
    }

    public List<E> topHalf() {

        List<E> res = new ArrayList<>();
        Deque<Node<E>> q = new LinkedList<>();
        double halfSize = (double) size() / 2;

        q.offer(root);

        while (!q.isEmpty() && res.size() < halfSize) {
            Node<E> n = q.poll();
            if (n != null) {
                res.add(n.getElement());
                q.offer(n.getLeft());
                q.offer(n.getRight());
            }
        }

        Collections.sort(res, Collections.reverseOrder());
        return res;
    }

}

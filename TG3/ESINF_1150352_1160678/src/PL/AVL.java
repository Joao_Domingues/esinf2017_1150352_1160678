/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PL;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;

/**
 *
 * @author DEI-ESINF
 * @param <E>
 */
public class AVL<E extends Comparable<E>> extends BST<E> {

    private int balanceFactor(Node<E> node) {
        return (height(node.getRight()) - height(node.getLeft()));
    }

    private Node<E> rightRotation(Node<E> node) {
        Node<E> leftson = node.getLeft();

        node.setLeft(leftson.getRight());
        leftson.setRight(node);

        node = leftson;

        return node;
    }

    private Node<E> leftRotation(Node<E> node) {
        Node<E> rightson = node.getRight();

        node.setRight(rightson.getLeft());
        rightson.setLeft(node);

        node = rightson;

        return node;
    }

    private Node<E> twoRotations(Node<E> node) {
        if (balanceFactor(node) < 0) {
            node.setLeft(leftRotation(node.getLeft()));
            node = rightRotation(node);
        } else {
            node.setRight(rightRotation(node.getRight()));
            node = leftRotation(node);
        }
        return node;
    }

    private Node<E> balanceNode(Node<E> node) {
        int bf = balanceFactor(node);
        
        if(bf < -1){
            int bfChild = balanceFactor(node.getLeft());
            if(bfChild < 0){
                node = rightRotation(node);
            } else if(bfChild > 0){
                node = twoRotations(node);
            }
        } else if(bf > 1){
            int bfChild = balanceFactor(node.getRight());
            if(bfChild > 0){
                node = leftRotation(node);
            } else if(bfChild < 0){
                node = twoRotations(node);
            }
        }
        return node;
    }

    @Override
    public void insert(E element) {
        root = insert(element, root);
    }

    private Node<E> insert(E element, Node<E> node) {
        if (node == null) {
            return new Node(element, null, null);
        }

        int cmp = element.compareTo(node.getElement());

        if (cmp == 0) {
            node.setElement(element);
        } else if (cmp < 0) {
            node.setLeft(insert(element, node.getLeft()));
            node = balanceNode(node);
        } else {
            node.setRight(insert(element, node.getRight()));
            node = balanceNode(node);
        }
        return node;
    }

    @Override
    public void remove(E element) {
        root = remove(element, root());
    }

    private Node<E> remove(E element, BST.Node<E> node) {
        if (node == null) {
            return null;
        }

        int cmp = element.compareTo(node.getElement());

        if (cmp == 0) {
            if (node.getLeft() == null && node.getRight() == null) {
                return null;
            }
            if (node.getLeft() == null) {
                return node.getRight();
            }
            if (node.getRight() == null) {
                return node.getLeft();
            }
            E smallElem = smallestElement(node.getRight());
            node.setElement(smallElem);
            node.setRight(remove(smallElem, node.getRight()));
            node = balanceNode(node);
        }
        else if(cmp < 0){
            node.setLeft(remove(element, node.getLeft()));
            node = balanceNode(node);
        } else {
            node.setRight(remove(element, node.getRight()));
            node = balanceNode(node);
        }
        return node;
    }
    
    @Override
    public boolean equals(Object obj){
        if(obj == null || obj.getClass() != this.getClass()) return false;
        
        AVL<E> outraAVL = (AVL<E>) obj;
        ArrayList<E> it =  (ArrayList<E>) outraAVL.inOrder();
        ArrayList<E> list = (ArrayList<E>) this.inOrder();
        Iterator<E> it2 = list.iterator();
        
        if(it.size() != list.size()) return false;
        
        for(E e : it){
            if(!e.equals(it2.next())){
                return false;
            }
        }
        return true;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esinf_1150352_1160678;

import PL.Arvores;
import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

/**
 *
 * @author HP
 */
public class LeituraFicheiros {

    private Scanner sc;
    private Arvores a;
    
    private final int LIMITE_UNIDADES = 9;
    private final int LIMITE_DEZENAS = 27;
    private final int LIMITE_CENTENAS = 9;

    public LeituraFicheiros(Arvores arvores) {
        a = arvores;
    }
    
    /**
     * Método que lê o ficheiro que contém a nomenclatura dos poligonos com apenas um digito de lados.
     * @param nomeFich Nome do ficheiro a ler.
     * @return boolean que indica se o ficheiro foi lido corretamente e se os dados foram inseridos.
     */
    public boolean lerUnidades(String nomeFich) {
        boolean flag;
        try {
            sc = new Scanner(new File(nomeFich));
            Poligono[] listaUnidades = new Poligono[LIMITE_UNIDADES];
            int i = 0;
            while (sc.hasNext()) {
                String linha = sc.nextLine();
                String[] dados = linha.split(";");
                int lados = Integer.parseInt(dados[0]);
                String designacao = dados[1];
                Poligono p = new Poligono(designacao, lados);
                listaUnidades[i] = p;
                i++;
            }
            flag = a.inserirUnidades(listaUnidades);
        } catch (FileNotFoundException ex) {
            flag = false;
            System.out.println(ex.getMessage());
        }
        return flag;
    }
    
    /**
     * Método que lê o ficheiro que contém a nomenclatura dos poligonos com dois digitos de lados.
     * @param nomeFich Nome do ficheiro a ler.
     * @return boolean que indica se o ficheiro foi lido corretamente e se os dados foram inseridos.
     */
    public boolean lerDezenas(String nomeFich){
        boolean flag;
        try{
            sc = new Scanner(new File(nomeFich));
            Poligono[] listaDezenas = new Poligono[LIMITE_DEZENAS];
            int i = 0;
            while(sc.hasNext()){
                String linha = sc.nextLine();
                String[] dados = linha.split(";");
                int lados = Integer.parseInt(dados[0]);
                Poligono p = new Poligono(dados[1], lados);
                listaDezenas[i] = p;
                i++;
            }
            flag = a.inserirDezenas(listaDezenas);
        } catch(FileNotFoundException ex){
            flag = false;
            System.out.println(ex.getMessage());
        }
        return flag;
    }
    
    /**
     * Método que lê o ficheiro que contém a nomenclatura dos poligonos com três digitos de lados.
     * @param nomeFich Nome do ficheiro a ler.
     * @return boolean que indica se o ficheiro foi lido corretamente e se os dados foram inseridos.
     */
    public boolean lerCentenas(String nomeFich){
        boolean flag;
        try{
            sc = new Scanner(new File(nomeFich));
            Poligono[] listaCentenas = new Poligono[LIMITE_CENTENAS];
            int i = 0;
            while(sc.hasNext()){
                String linha = sc.nextLine();
                String[] dados = linha.split(";");
                int lados = Integer.parseInt(dados[0]);
                Poligono p = new Poligono(dados[1], lados);
                listaCentenas[i] = p;
                i++;
            }
            flag = a.inserirCentenas(listaCentenas);
        } catch(FileNotFoundException ex){
            flag = false;
            System.out.println(ex.getMessage());
        }
        return flag;
    }
    
    /**
     * Método que preenche um arrayList com todos os poligonos de 1 a 999 lados
     * @param nomeFich Nome do ficheiro de onde é lida a informação
     * @return retorna uma lista de poligonos ou uma lista vazia se a informação do ficheiro não for válida.
     */
    public ArrayList<Poligono> lerTodosPoligonos(String nomeFich){
        ArrayList<Poligono> todosPoligonos = new ArrayList<>();
        boolean flag = true;
        try{
            sc = new Scanner(new File(nomeFich));
            while(sc.hasNext()){
                String linha = sc.nextLine();
                String[] dados = linha.split(";");
                int lados = Integer.parseInt(dados[0]);
                Poligono p = new Poligono(dados[1], lados);
                todosPoligonos.add(p);
            }
        } catch (FileNotFoundException ex) {
            flag = false;
            System.out.println(ex.getMessage());
        }
        if(flag == false){
            Iterator<Poligono> it = todosPoligonos.iterator();
            while(it.hasNext()){
                todosPoligonos.remove(it.next());
            }
        }
        return todosPoligonos;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esinf_1150352_1160678;

/**
 *
 * @author HP
 */
public class Poligono implements Comparable<Poligono>{
    public String designacao;
    public int lados;
    
    public Poligono(String designacao, int lados){
        this.designacao = designacao;
        this.lados = lados;
    }
    
    @Override
    public boolean equals(Object obj){
        if(obj == null || this.getClass() != obj.getClass()) return false;
        
        Poligono outroPoligono = (Poligono) obj;
        return this.lados == outroPoligono.lados;
    }
    
    @Override
    public String toString(){
        return lados + ": " + designacao;
    }

    /**
     * @return the designacao
     */
    public String getDesignacao() {
        return designacao;
    }

    /**
     * @return the lados
     */
    public int getLados() {
        return lados;
    }

    @Override
    public int compareTo(Poligono o) {
        if(this.lados < o.lados) return -1;
        if(this.lados > o.lados) return 1;
        else return 0;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esinf_1150352_1160678;

import PL.AVL;
import PL.Arvores;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author HP
 */
public class Main {
    public static void main(String[] args){
        
        //ALÍNEA A:
        Arvores a = new Arvores();
        LeituraFicheiros lf = new LeituraFicheiros(a);
        lf.lerUnidades("poligonos_prefixo_unidades.txt");
        lf.lerDezenas("poligonos_prefixo_dezenas.txt");
        lf.lerCentenas("poligonos_prefixo_centenas.txt");
        
        AVL<Poligono> unidades = a.getUnidades();
        AVL<Poligono> dezenas = a.getDezenas();
        AVL<Poligono> centenas = a.getCentenas();
        
        //Mostrar as três arvores
        System.out.println("ALÍNEA A:\n\n");
        System.out.println("Arvore das Unidades:\n");
        System.out.println("------------------------\n\n");
        System.out.println(unidades.toString());
        
        System.out.println("\n\nArvore das Dezenas:\n");
        System.out.println("------------------------\n\n");
        System.out.println(dezenas.toString());
        
        System.out.println("\n\nArvore das Centenas:\n");
        System.out.println("-------------------------\n\n");
        System.out.println(centenas.toString());
        
        
        //ALÍNEA B:
        System.out.println("\n\nALÍNEA B:\n\n");
        int lados = 524;
        System.out.println("Lados: 524"
                + "\nDesignação: " + a.nomePoligonoDeXLados(lados));
        
        lados = 15;
        System.out.println("\n\nLados: 15"
                + "\nDesignação: " + a.nomePoligonoDeXLados(lados));
        
        lados = 995;
        System.out.println("\n\nLados: 995"
                + "\nDesignação: " + a.nomePoligonoDeXLados(lados));
        
        
        //ALÍNEA C:
        System.out.println("\n\nALÍNEA C:\n\n");
        AVL<Poligono> arvoreCompleta = a.todosPoligonos();
        
        System.out.println(arvoreCompleta.toString());
        
        
        //ALÍNEA D:
        System.out.println("\n\nALÍNEA D:\n\n");
        a.setCompleto(arvoreCompleta);
        String designacao = "hectatriacontaoctagon"; //138
        System.out.println("Designação: hectatriacontaoctagon"
                + "\nLados: " + String.valueOf(a.numeroDeLadosDePoligono(designacao)));
        
        designacao = "tetrahectaheptacontadigon"; //472
        System.out.println("\n\nDesignação: tetrahectaheptacontadigon"
                + "\nLados: " + String.valueOf(a.numeroDeLadosDePoligono(designacao)));
        
        designacao = "heptahectaheptacontaoctagon"; //778
        System.out.println("\n\nDesignação: heptahectaheptacontaoctagon"
                + "\nLados: " + String.valueOf(a.numeroDeLadosDePoligono(designacao)));
        
        
        //ALÍNEA E:
        System.out.println("\n\nALÍNEA E:\n\n");
        List<String> subList = new ArrayList<>();
        int lado1 = 437;
        int lado2 = 444;
        subList = a.listaIntervalo(lado1, lado2);
        System.out.println(subList.toString());
        
        //ALÍNEA F:
        System.out.println("\n\nALÍNEA F:\n\n");
        Poligono p1 = new Poligono("henagon", 1);
        Poligono p2 = new Poligono("enneahectaenneacontaenneagon", 999);
        Poligono antecessorComum = a.lowestCommonAncestor(p1, p2);//512
        System.out.println("Antecessor Comum mais próximo entre poligono de lados 1 e 999:\n"
                + antecessorComum.toString());
        
        p2 = new Poligono("enneahectaenneacontaenneagon", 999);
        p1 = new Poligono("enneahectaenneacontaheptagon", 997);
        antecessorComum = a.lowestCommonAncestor(p1, p2);//998
        System.out.println("Antecessor Comum mais próximo entre poligono de lados 997 e 999:\n"
                + antecessorComum.toString());
    }
}

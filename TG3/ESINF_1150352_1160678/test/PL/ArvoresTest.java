/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PL;

import esinf_1150352_1160678.LeituraFicheiros;
import esinf_1150352_1160678.Poligono;
import java.util.*;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author HP
 */
public class ArvoresTest {

    private LeituraFicheiros lf;
    private AVL<Poligono> arvoreUnidades;
    private AVL<Poligono> arvoreDezenas;
    private AVL<Poligono> arvoreCentenas;
    private AVL<Poligono> todosPoligonos;
    private Arvores a;

    public ArvoresTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        a = new Arvores();
        lf = new LeituraFicheiros(a);
        lf.lerUnidades("poligonos_prefixo_unidades.txt");
        lf.lerDezenas("poligonos_prefixo_dezenas.txt");
        lf.lerCentenas("poligonos_prefixo_centenas.txt");
        arvoreUnidades = a.getUnidades();
        arvoreDezenas = a.getDezenas();
        arvoreCentenas = a.getCentenas();
        todosPoligonos = new AVL<>();
        
        ArrayList<Poligono> todosOsPoligonos = new ArrayList<>();
        todosOsPoligonos = lf.lerTodosPoligonos("teste_lados_nome.txt");
        for(Poligono p : todosOsPoligonos){
            todosPoligonos.insert(p);
        }
        a.setCompleto(todosPoligonos);
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of inserirUnidades method, of class Arvores.
     */
    @Test
    public void testInserirUnidades() {
        System.out.println("inserirUnidades");
        Poligono[] listaUnidades2 = new Poligono[9];
        listaUnidades2[0] = new Poligono("hena", 1);
        listaUnidades2[1] = new Poligono("di", 2);
        listaUnidades2[2] = new Poligono("tri", 3);
        listaUnidades2[3] = new Poligono("tetra", 4);
        listaUnidades2[4] = new Poligono("penta", 5);
        listaUnidades2[5] = new Poligono("hexa", 6);
        listaUnidades2[6] = new Poligono("hepta", 7);
        listaUnidades2[7] = new Poligono("octa", 8);
        listaUnidades2[8] = new Poligono("ennea", 9);
        boolean expResult = true;
        boolean result = a.inserirUnidades(listaUnidades2);
        assertEquals(expResult, result);

        Poligono[] listaUnidades3 = new Poligono[8];
        listaUnidades2[0] = new Poligono("hena", 1);
        listaUnidades2[1] = new Poligono("di", 2);
        listaUnidades2[2] = new Poligono("tri", 3);
        listaUnidades2[3] = new Poligono("tetra", 4);
        listaUnidades2[4] = new Poligono("penta", 5);
        listaUnidades2[5] = new Poligono("hexa", 6);
        listaUnidades2[6] = new Poligono("hepta", 7);
        listaUnidades2[7] = new Poligono("octa", 8);

        expResult = false;
        result = a.inserirUnidades(listaUnidades3);
        assertEquals(expResult, result);
        
    }

    /**
     * Test of inserirDezenas method, of class Arvores.
     */
    @Test
    public void testInserirDezenas() {
        System.out.println("inserirDezenas");
        Poligono[] listaDezenas2 = new Poligono[26];
        listaDezenas2[0] = new Poligono("deca", 10);
        listaDezenas2[1] = new Poligono("hendeca", 11);
        listaDezenas2[2] = new Poligono("dodeca", 12);
        listaDezenas2[3] = new Poligono("triskaideca", 13);
        listaDezenas2[4] = new Poligono("tetrakaideca", 14);
        listaDezenas2[5] = new Poligono("pentakaideca", 15);
        listaDezenas2[6] = new Poligono("hexakaideca", 16);
        listaDezenas2[7] = new Poligono("heptakaideca", 17);
        listaDezenas2[8] = new Poligono("octakaideca", 18);
        listaDezenas2[9] = new Poligono("enneakaideca", 19);
        listaDezenas2[10] = new Poligono("icosa", 20);
        listaDezenas2[11] = new Poligono("icosihena", 21);
        listaDezenas2[12] = new Poligono("icosidi", 22);
        listaDezenas2[13] = new Poligono("icositri", 23);
        listaDezenas2[14] = new Poligono("icositetra", 24);
        listaDezenas2[15] = new Poligono("icosipenta", 25);
        listaDezenas2[16] = new Poligono("icosihexa", 26);
        listaDezenas2[17] = new Poligono("icosihepta", 27);
        listaDezenas2[18] = new Poligono("icosiocta", 28);
        listaDezenas2[19] = new Poligono("icosiennea", 29);
        listaDezenas2[20] = new Poligono("triaconta", 30);
        listaDezenas2[21] = new Poligono("tetraconta", 40);
        listaDezenas2[22] = new Poligono("pentaconta", 50);
        listaDezenas2[23] = new Poligono("hexaconta", 60);
        listaDezenas2[24] = new Poligono("heptaconta", 70);
        listaDezenas2[25] = new Poligono("octaconta", 80);
        Arvores instance = new Arvores();
        boolean expResult = false;
        boolean result = instance.inserirDezenas(listaDezenas2);
        assertEquals(expResult, result);

        Poligono[] listaDezenas3 = new Poligono[27];
        listaDezenas3[0] = new Poligono("deca", 10);
        listaDezenas3[1] = new Poligono("hendeca", 11);
        listaDezenas3[2] = new Poligono("dodeca", 12);
        listaDezenas3[3] = new Poligono("triskaideca", 13);
        listaDezenas3[4] = new Poligono("tetrakaideca", 14);
        listaDezenas3[5] = new Poligono("pentakaideca", 15);
        listaDezenas3[6] = new Poligono("hexakaideca", 16);
        listaDezenas3[7] = new Poligono("heptakaideca", 17);
        listaDezenas3[8] = new Poligono("octakaideca", 18);
        listaDezenas3[9] = new Poligono("enneakaideca", 19);
        listaDezenas3[10] = new Poligono("icosa", 20);
        listaDezenas3[11] = new Poligono("icosihena", 21);
        listaDezenas3[12] = new Poligono("icosidi", 22);
        listaDezenas3[13] = new Poligono("icositri", 23);
        listaDezenas3[14] = new Poligono("icositetra", 24);
        listaDezenas3[15] = new Poligono("icosipenta", 25);
        listaDezenas3[16] = new Poligono("icosihexa", 26);
        listaDezenas3[17] = new Poligono("icosihepta", 27);
        listaDezenas3[18] = new Poligono("icosiocta", 28);
        listaDezenas3[19] = new Poligono("icosiennea", 29);
        listaDezenas3[20] = new Poligono("triaconta", 30);
        listaDezenas3[21] = new Poligono("tetraconta", 40);
        listaDezenas3[22] = new Poligono("pentaconta", 50);
        listaDezenas3[23] = new Poligono("hexaconta", 60);
        listaDezenas3[24] = new Poligono("heptaconta", 70);
        listaDezenas3[25] = new Poligono("octaconta", 80);
        listaDezenas3[26] = new Poligono("enneaconta", 90);

        expResult = true;
        result = instance.inserirDezenas(listaDezenas3);
        assertEquals(expResult, result);
    }

    /**
     * Test of inserirCentenas method, of class Arvores.
     */
    @Test
    public void testInserirCentenas() {
        System.out.println("inserirCentenas");
        Poligono[] listaCentenas2 = new Poligono[9];
        listaCentenas2[0] = new Poligono("hecta", 100);
        listaCentenas2[1] = new Poligono("dihecta", 200);
        listaCentenas2[2] = new Poligono("trihecta", 300);
        listaCentenas2[3] = new Poligono("tetrahecta", 400);
        listaCentenas2[4] = new Poligono("pentahecta", 500);
        listaCentenas2[5] = new Poligono("hexahecta", 600);
        listaCentenas2[6] = new Poligono("heptahecta", 700);
        listaCentenas2[7] = new Poligono("octahecta", 800);
        listaCentenas2[8] = new Poligono("enneahecta", 900);
        Arvores instance = new Arvores();
        boolean expResult = true;
        boolean result = instance.inserirCentenas(listaCentenas2);
        assertEquals(expResult, result);

        Poligono[] listaCentenas3 = new Poligono[8];
        listaCentenas3[0] = new Poligono("hecta", 100);
        listaCentenas3[1] = new Poligono("dihecta", 200);
        listaCentenas3[2] = new Poligono("trihecta", 300);
        listaCentenas3[3] = new Poligono("tetrahecta", 400);
        listaCentenas3[4] = new Poligono("pentahecta", 500);
        listaCentenas3[5] = new Poligono("hexahecta", 600);
        listaCentenas3[6] = new Poligono("heptahecta", 700);
        listaCentenas3[7] = new Poligono("octahecta", 800);
        expResult = false;
        result = instance.inserirCentenas(listaCentenas3);
        assertEquals(expResult, result);
    }

    /**
     * Test of todosPoligonos method, of class Arvores.
     */
    @Test
    public void testTodosPoligonos() {
        System.out.println("todosPoligonos");
        AVL<Poligono> expResult = todosPoligonos;
        AVL<Poligono> result = a.todosPoligonos();
        assertEquals(expResult, result);
        
    }
    
    /**
     * Test of nomePoligonosDeXLados method, of class Arvores.
     */
    @Test
    public void testNomePoligonoDeXLados() {
        System.out.println("nomePoligonoDeXLados");
        
        Arvores teste = new Arvores();
        
        // teste com uma instância da classe Arvores com arvores vazias
        String result = teste.nomePoligonoDeXLados(3);
        String expResult = "";
        assertEquals(expResult, result);
        
        // teste com uma instância da classe Arvores com árvores válidas e completas, mas com o número de lados inválido
        result = a.nomePoligonoDeXLados(0);
        assertEquals(expResult, result);
        
        // teste com uma instância da classe Arvores com árvores válidas e completas, mas com o número de lados inválido
        result = a.nomePoligonoDeXLados(-1);
        assertEquals(expResult, result);
        
        // teste de sucesso
        result = a.nomePoligonoDeXLados(3);
        expResult = "trigon";
        assertEquals(expResult, result);
        
        // teste de sucesso
        result = a.nomePoligonoDeXLados(11);
        expResult = "hendecagon";
        assertEquals(expResult, result);
        
        // teste de sucesso
        result = a.nomePoligonoDeXLados(32);
        expResult = "triacontadigon";
        assertEquals(expResult, result);
        
        // teste de sucesso
        result = a.nomePoligonoDeXLados(534);
        expResult = "pentahectatriacontatetragon";
        assertEquals(expResult, result);
        
        // teste de sucesso
        result = a.nomePoligonoDeXLados(611);
        expResult = "hexahectahendecagon";
        assertEquals(expResult, result);
    }

    /**
     * Test of numeroDeLadosDePoligono method, of class Arvores.
     */
    @Test
    public void testNumeroDeLadosDePoligono() {
        System.out.println("numeroDeLadosDePoligono");
        
        Arvores teste = new Arvores();
        
        // teste com uma instância da classe Arvores com arvores vazias
        int result = teste.numeroDeLadosDePoligono("trigon");
        int expResult = 0;
        assertEquals(expResult, result);
        
        // teste com uma instância da classe Arvores com árvores válidas e completas, mas com o nome inválido
        result = a.numeroDeLadosDePoligono("");
        assertEquals(expResult, result);
        
        // teste com uma instância da classe Arvores com árvores válidas e completas, mas com o nome inválido
        result = a.numeroDeLadosDePoligono(null);
        assertEquals(expResult, result);
        
        // teste de sucesso
        result = a.numeroDeLadosDePoligono("trigon");
        expResult = 3;
        assertEquals(expResult, result);
        
        // teste de sucesso
        result = a.numeroDeLadosDePoligono("hendecagon");
        expResult = 11;
        assertEquals(expResult, result);
        
        // teste de sucesso
        result = a.numeroDeLadosDePoligono("triacontadigon");
        expResult = 32;
        assertEquals(expResult, result);
        
        // teste de sucesso
        result = a.numeroDeLadosDePoligono("pentahectatriacontatetragon");
        expResult = 534;
        assertEquals(expResult, result);
        
        // teste de sucesso
        result = a.numeroDeLadosDePoligono("hexahectahendecagon");
        expResult = 611;
        assertEquals(expResult, result);
    }
    
    /**
     * Test of lowestCommonAncestor method, of class Arvores.
     */
    @Test
    public void testLowestCommonAncestor() {
        System.out.println("lowestCommonAncestor");
        
        Arvores teste = new Arvores();
        Poligono poli1 = new Poligono("hexahectaicosioctagon", 628);
        Poligono poli2 = new Poligono("dihectaoctagon", 208);
        
        // teste com uma instância da classe Arvores com arvores vazias
        Poligono result = teste.lowestCommonAncestor(poli1, poli2);
        Poligono expResult = null;
        assertEquals(expResult, result);
        
        // teste com uma instância da classe Arvores com árvores válidas e completas, mas com os poligonos inválidos
        result = a.lowestCommonAncestor(poli1, null);
        assertEquals(expResult, result);
        
        // teste com uma instância da classe Arvores com árvores válidas e completas, mas com os poligonos inválidos
        result = a.lowestCommonAncestor(null, poli2);
        assertEquals(expResult, result);
        
        // teste de sucesso
        result = a.lowestCommonAncestor(poli1, poli2);
        expResult = new Poligono("pentahectadodecagon", 512);
        assertEquals(expResult, result);
        
        // teste de sucesso, em que um dos poligonos é o lowestcommonancestor
        poli2 = new Poligono("pentahectadodecagon", 512);
        result = a.lowestCommonAncestor(poli1, poli2);
        expResult = new Poligono("pentahectadodecagon", 512);
        assertEquals(expResult, result);
        
        // teste de sucesso, em que os dois polígonos são o mesmo
        poli1 = new Poligono("henagon", 1);
        poli2 = new Poligono("henagon", 1);
        result = a.lowestCommonAncestor(poli1, poli2);
        expResult = new Poligono("henagon", 1);
        assertEquals(expResult, result);
        
        // teste de sucesso
        poli1 = new Poligono("enneahectaenneacontaenneagon", 999);
        poli2 = new Poligono("997;enneahectaenneacontaheptagon", 997);
        result = a.lowestCommonAncestor(poli1, poli2);
        expResult = new Poligono("enneahectaenneacontaenneagon", 998);
        assertEquals(expResult, result);
    }

    /**
     * Test of listaIntervalo method, of class Arvores.
     */
    @Test
    public void testListaIntervalo() {
        System.out.println("listaIntervalo");
        int lados1 = 0;
        int lados2 = 0;
        List<String> expResult = null;
        List<String> result = a.listaIntervalo(lados1, lados2);
        assertEquals(expResult, result);
        
        lados2 = 1300;
        result = a.listaIntervalo(lados1, lados2);
        assertEquals(expResult, result);
        
        lados1 = -1;
        result = a.listaIntervalo(lados1, lados2);
        assertEquals(expResult, result);
        
        lados1 = 1;
        lados2 = 3;
        result = a.listaIntervalo(lados1, lados2);
        expResult = new ArrayList<>();
        expResult.add("henagon");
        expResult.add("digon");
        expResult.add("trigon");
        System.out.println(result.toString());
        Collections.reverse(expResult);
        assertEquals(expResult, result);
        
        lados1 = 997;
        lados2 = 999;
        result = a.listaIntervalo(lados1, lados2);
        List<String> expResult1 = new ArrayList<>();
        expResult1.add("enneahectaenneacontaenneagon");
        expResult1.add("enneahectaenneacontaoctagon");
        expResult1.add("enneahectaenneacontaheptagon");
        assertEquals(expResult1, result);
    }
}
